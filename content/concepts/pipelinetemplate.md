+++
title = "流水线模板"
description = ""
weight = 2
alwaysopen = false
+++

## 概述

模板是为了解决用户手动编写 Jenkinsfile 难度较高而存在的。  
用户只需要 填写模板定义好的参数，就能够快速生成一套 Jenkinsfile。  
在平台上的体现为： 用户可以选择已有的模板，填写参数，保存后，即可完成流水线的创建。  

为了使独立的逻辑块能够尽可能复用， 我们抽象了 TaskTemplate 和 PipelineTemplate， TaskTemplate 可以通过组合形成 PipelineTemplate.

## 模板的分类

根据 K8S的资源类型来划分， 分为以下类别：

- `TaskTemplate`: Namespaced 级别的 任务模板
- `PipelineTemplate`: Namespaced 级别的 流水线模板
- `ClusterTaskTemplate`: 平台级别的 任务模板
- `ClusterPipelineTemplate`: 平台级别的 流水线模板

也就是说，可以通过命令 `kubectl get tasktemplate -n {namespace}` 来获取集群内存储的资源。对于 Cluster级别的资源，属于所有 namespace 共有。

按照来源， 分为一下类别
- `official`: 官方模板
- `customer`： 客户自定义模板
会使用模板的 label 进行标识。 例如：

```
kind: ClusterPipelineTemplate
metadata:
  name: GolangBuild
  labels:
    source=official

---
kind: ClusterPipelineTemplate
metadata:
  name: GolangBuild
  labels:
    source=customer
```

在业务定义上， 官方模板， 一定是 Cluster 级别的，客户的自定义模板，有可能是  Cluster级别的，也有可能是namespace 级别的。

使用图表来表示

| \ | `source=official` | `source=customer` |
|-----|----|---|
| Cluster| 官方模板|平台级别自定义模板|
| Namespaced| 无 |项目级别的自定义模板|

目前，产品上并没有将 `自定义的平台级别的模板` 产品化，也就是说，没有相应的入口。

## 模板的管理

目前， 模板只能从代码仓库导入到平台中。

### 项目级别的模板导入

在使用平台，进行项目级别的模板导入时， 会生成 `PipelineTemplateSync` 的资源，devops-controller 会根据资源的定义， 拉取代码， 读取模板文件，导入到平台中。

例如：
``` yaml
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineTemplateSync
metadata:
  name: TemplateSync
  namespace: xxx
spec:
  source:
    # 绑定的代码仓库
    codeRepository:
      name: bitbucket-mathildetech-templates
      ref: master
    # 手动输入的代码仓库地址
    git:
      ref: master
      uri: https://alaudabot@bitbucket.org/mathildetech/xxxxx.git
    # 仓库对应的凭据
    secret:
      name: devops-bitbucket
      namespace: global-credentials
    sourceType: "GIT"
```


### 平台级别的模板导入

目前该功能只是内部体验功能， 并没有提供对应的产品化功能.

进行平台级别的模板导入时， 会生成 `ClusterPipelineTemplateSync` 的资源，devops-controller 会根据资源的定义， 拉取代码， 读取模板文件，导入到平台中。

例如：
``` yaml
apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplateSync
metadata:
  name: TemplateSync
  namespace: xxx
spec:
  source:
    # 绑定的代码仓库
    codeRepository:
      name: bitbucket-mathildetech-templates
      ref: master
    # 手动输入的代码仓库地址
    git:
      ref: master
      uri: https://alaudabot@bitbucket.org/mathildetech/xxxx.git
    # 仓库对应的凭据
    secret:
      name: devops-bitbucket
      namespace: global-credentials
    sourceType: "GIT"
```

### 官方模板导入

官方模板导入后，会生成一个 `TemplateSyncOfficial` 的资源。可以通过 

```bash
kubectl get clusterpipelinetemplatesync TemplateSyncOfficial -o yaml 
```
查看导入的详细信息。 Conditions 中保存的有 所有模板的导入结果。


## 模板的版本管理
