+++
title = "核心概念"
description = ""
weight = 2
alwaysopen = false
+++

## 资源列表

### 工具链集成与绑定

**集成**

为了帮助用户管理不同的工具的实例以及提供客观的工具状态，DevOps APIService抽象了如下资源类型

|资源类型|描述|Scope|
|-----|----|----|
|`Jenkins`| Jenkins 工具抽象|cluster|
|`CodeRepoService`|代码托管工具抽象， 例如 gitlab，github 等|cluster|
|`ImageRegistry`|	 docker image registry工具抽象|cluster|
|`ProjectManagement`|	项目管理工具抽象	|cluster|
|`CodeQualityTool`|	代码扫描工具抽象, 例如 sonarqube|cluster|
|`ArtifactRegistryManager`|	制品仓库管理工具， 例如 nexus| cluster |
|`ArtifactRegistry`|	制品仓库管理工具内，某种细分类型，比如maven,docker等| cluster |

**绑定**

为了方便控制不同的用户使用或访问工具，又抽象绑定工具到 namespace 的资源

|资源类型|描述|Scope|
|-----|----|----|
|`JenkinsBinding`| 在对应的Jenkins实例创建namespace统一名称的目录来隔离流水线的访问 |	Namespaced|
|`CodeRepoBinding`|	用户可以指定一或多个org/team以及org/team下的代码仓库来同步到同一个namespace下 |	Namespaced|
|`ImageRepoBinding`| 用户可以指定一或多个仓库/仓库目录，以及仓库/仓库目录下的镜像仓库来同步到同一个namespace下 |	Namespaced|
|`ProjectManagementBinding`|用户可以指定一或多个项目，目前未实现同步资源的部分 |	Namespaced|
|`DocumentManagementBinding`|	用户可以指定一或多个Space，目前未实现同步资源的部分 |	Namespaced|
|`CodeQualityBinding`| 用户指定绑定策略。目前支持代码仓库为主（同步namespace下已绑定代码仓库的扫描结果） |	Namespaced|
|`ArtifactRegistryBinding`|用户指定一或多个项目  |	Namespaced|

**流水线**

为了帮助用户快速达到CI/CD目标，流水线作为核心DevOps功能。 除了Jenkins和JenkinsBinding之外，流水线功能包含如下资源类型：

|资源类型|描述|Scope|
|-----|----|----|
|`PipelineConfig`|	描述一个流水线的配置。会自动同步到Jenkins中为Job	| Namespaced|
|`Pipeline`|	记录一个流水线的执行记录。会自动从往jenkins同步为build	| Namespaced|
|`PipelineTemplateSync`|	同步一个代码仓库的流水线模版到某个项目下，成为当前项目下的私有模板。需要配置一个代码仓库地址，secret信息（代码仓库凭据）	| Namespaced|
|`ClusterPipelineTemplateSync`|	同步一个代码仓库的流水线模版到所有项目中， 将会成为全局的流水线模板。需要配置一个代码仓库地址，secret信息（代码仓库凭据）	| Namespaced|
|`PipelineTemplate`|	流水线模版	| Namespaced|
|`PipelineTemplateTask`|	流水线模版中的任务	| Namespaced|
|`ClusterPipelineTemplate`|	集群全局流水线模版	| Cluster|
|`ClusterPipelineTaskTemplate`|	集群全局流水线模版任务	| Cluster|

**代码托管**

除了CodeRepoService和CodeRepoBinding之外，代码托管功能包含如下资源类型：

|资源类型|描述|Scope|
|-----|----|----|
|`CodeRepository` |	描述代码托管服务中的一个代码仓库|	Namespaced|

**镜像仓库**

除了ImageRegistry和ImageRepoBinding之外，制品仓库功能包含如下资源类型：

|资源类型|描述|Scope|
|-----|----|----|
|`ImageRepository` |	描述镜像仓库，以及tag相关的信息	|	Namespaced|

**代码扫描**

除了CodeQualityService 和CodeQualityBinding 之外，制品仓库功能包含如下资源类型：

|资源类型|描述|Scope|
|-----|----|----|
|`CodeQualityProject` |	描述代码质量服务中的项目信息	|	Namespaced|

**其他**

ArtifactRegistryManager会创建/集成不同的仓库，不同的仓库对应不同的资源类型

|工具|仓库类型|对应资源类型|
|-----|----|----|
| Nexus | Maven2 | `ArtifactRegistry` |
| Jfrog-Artifactory | Docker | `ImageRegistry` |