+++
title = "自定义流水线模板"
description = ""
weight = 8
alwaysopen = false
+++

## 参考文档

- [流水线模板概念](/concepts/pipelinetemplate/)
- [流水线模板语法说明](/components/devops-apiserver/pipelinetemplate/)
- [流水线模板功能设计文档](/feature/pipelinetemplate/default/)