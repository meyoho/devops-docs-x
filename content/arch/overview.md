+++
title = "概览"
description = "架构概览"
weight = 1
alwaysopen = false
+++

#### 总体架构

![overview](/images/devops-arch-background.png)

说明  

- `diablo-clone`: 前端UI组件, 请求 `API Gateway`实现平台的功能。部署在Global集群。
- `ApiGateway`: 平台 APIGateway组件，负责转发 前端UI组件的请求到后端API组件。部署在Global集群。
- `DevOpsAPI`: DevOps的高级API，调用 `DevOps APIServer`对获得的数据的进行一定封装处理。部署在Global集群。
- `DevOpsAPIServer`: DevOps的API Server，标准的k8s aggregation api server。 负责资源的增删改查。同时封装了第三方工具的API. 部署在Global集群。
- `DevOpsController`: DevOps的controller，监听或调用DevOps-APIServer的API, 负责进行资源的同步，保证目标资源的状态符合预期。部署在Global集群。
- `erebus`: 多集群管理组件，负责多集群的kube-apiserver的代理。 部署在Global集群。
- `jenkins`: 持续集成工具jenkins， 部署在业务集群。
- `nexus`: 制品仓库管理工具nexus， 部署在业务集群。
- `Gitlab`: 代码仓库工具Gitlab， 部署在业务集群。
- `Harbor`: docker镜像仓库管理具， 部署在业务集群。
- `SonarQube`: 代码扫描工具， 部署在业务集群。

