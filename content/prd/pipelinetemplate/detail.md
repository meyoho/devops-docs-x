+++
title = "流水线模板"
description = ""
weight = 2
alwaysopen = true
+++

### 功能描述

流水线模板实现了两部分， 用户的自定义模板和官方模板，用户可使用项目中的流水线模板快速创建流水线。

- 自定义模板：用户为满足自身业务需求，根据流水线模板规范，可编写符合业务需求的模板；

- 官方模板：DevOps官方提供的流水线模板，可满足一些通用的业务需求；


### 软件界面设计

![pipelinetemplate-1](/images/ui/pipelinetemplate-1.png)
