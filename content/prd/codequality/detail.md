+++
title = "代码扫描"
description = ""
weight = 2
alwaysopen = true
+++

#### 功能描述

检测代码的规范性、代码缺陷、漏洞、坏味道、重复率等信息，提高代码质量。

- 管理视图

可在工具链集成绑定sonarqube等代码扫描工具到项目；

- 业务视图

可在业务视图“代码质量分析”查看代码扫描结果；


### 软件界面设计

![codequality1](/images/ui/codequality-1.png)
