+++
title = "镜像仓库"
description = ""
weight = 2
alwaysopen = true
+++

### 功能描述
镜像仓库功能主要有：

- 平台管理员或项目经理可以在平台管理视角，为平台集成镜像仓库。

- 平台管理员以项目为单位，使用已经集成的镜像仓库进行镜像仓库账号绑定，并且可以分配已绑定账号下的镜像仓库到指定对应项目。

- Harbor的项目是registry下的一层目录，为统一性考虑，可以和docker registry一样，以镜像仓库地址的形式展示。

- Harbor可以设置整个registry下的所有仓库为“只读”权限，为了已于用户了解当前仓库状态，可展示是否为只读状态。

- 支持镜像扫描，提供扫描结果概览，后期会增加扫描详情页（由于扫描结果页的地址没有api可以直接获取，暂不提供跳转查看）。

### 软件界面设计

![dockerimage1](/images/ui/dockerimage-1.png)
![dockerimage2](/images/ui/dockerimage-2.png)
