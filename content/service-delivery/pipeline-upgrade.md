+++
title = "如何手工进行流水线升级"
description = ""
weight = 1
alwaysopen = false
+++

#### 问题描述

由于平台升级，流水线模板升级后， 旧的流水线使用的还是旧的流水线模板，新的功能无法应用，需要对单个流水线进行升级。  
目前没有产品化的方案，来进行升级，但可以通过 命令行，手工来进行单个流水线的升级。  
升级之后， 对应的流水线会使用最新的模板，启用最新模板包含的功能。

#### 手工操作

确认所在的项目<namespace>，流水线的名称<configName>。  
在 global 集群执行命令 `kubectl edit pipelineconfig <config-name> -n <namespace>`  
可以看到 如下的yaml  

```
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineConfig
metadata:
  annotations:
    ...
  labels:
    ...
  name: test-demo1
  namespace: jtcheng
spec:
  ...
  template:
    graphValues: null
    pipelineTemplateRef:
      kind: PipelineTemplate
      name: Jmeter.1.0.3   # 定位这个值
      namespace: jtcheng
```

将 `spec.template.pipelineTemplateRef.name` 中的版本号后缀移除, 例如上例中

```
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineConfig
metadata:
  annotations:
    ...
  labels:
    ...
  name: test-demo1
  namespace: jtcheng
spec:
  ...
  template:
    graphValues: null
    pipelineTemplateRef:
      kind: PipelineTemplate
      name: Jmeter   # 去掉版本号，只保留原来的模板名称。
      namespace: jtcheng
```


这样，当前流水线使用的模板，即可以升级为对应模板的最新版本。  
保存成功后，可以通过  `kubectl get pipelineconfig <config-name> -n <namespace> -o yaml` 查看`spec.template.pipelineTemplateRef.name`字段的值是否自动变化为对应模板的最新版本。  
例如保存后，再次查看 yaml：

```
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineConfig
  ...
spec:
  ...
  template:
    graphValues: null
    pipelineTemplateRef:
      kind: PipelineTemplate
      name: Jmeter.1.0.4   # 会自动变成最新的版本号
      namespace: jtcheng
```


模板升级成功后， 通过 `kubectl get pipelineconfig <config-name> -n <namespace>` 确认流水线的状态是否变为 `Ready`. 

#### 可能的问题及风险

由于新版本的模板中，可能添加了新的参数或者逻辑，会导致升级到新版本后，出现不兼容的情况。 需要版本保存后，到页面上，更新对应的流水线，补充填写需要的参数，再次保存。