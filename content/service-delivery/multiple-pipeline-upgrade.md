+++
title = "如何手工进行批量流水线升级"
description = ""
weight = 1
alwaysopen = false
+++

#### 问题描述

由于平台升级，流水线模板升级后， 旧的流水线使用的还是旧的流水线模板，新的功能无法应用，多个流水线，如果单个升级的话，会很麻烦，所以我们需要一个快捷的方式对多个流水线进行批量升级。  

目前没有产品化的方案，来进行升级，但可以通过 API，手工来进行批量流水线的升级。  

升级之后， 对应的流水线会使用最新的模板，启用最新模板包含的功能。



#### 实现逻辑

对需要升级的流水线进行遍历，并以以下逻辑处理
* 先对流水线做 validate 的处理，尝试去生成新模板对应的jenkinsfile
* 如果 validate 处理成功，并且允许升级，则会对该流水线进行升级
* 如果失败，则会忽略对该流水线的升级
* 无论成功失败，都会将处理结果通知给用户


#### 手工操作

##### 需要信息

推荐使用postman,通过直接访问API来批量升级流水线，需要提前获取以下三个信息

* acp-devops-endpoint: 在浏览器中打开 ACP DevOps,打开开发者工具,点击任意按钮发送请求,查看发送请求中使用的API地址，一般这个地址和平台访问地址一致

* token: 在浏览器中打开 ACP DevOps,打开开发者工具,点击任意按钮发送请求,查看发送请求中使用的Bearer Token

* namespace: 在哪个命名空间中进行批量流水线升级

##### API定义

API: `https://{acp-devops-endpoint}/devops/api/v1/pipelineconfig/{namespace}/upgrade`

Method: POST

Authorization: token

Body: 
```
{
    "dryrun":true,
    "names":[
    ]
}
```

* 如果dryrun为true, 则只做数据的校验，不做实际升级

* 如果需要更新某个命名空间下所有的流水线，则names置空。如果需要更新某个命名空间下的某些流水线，则在names中写明流水线名称，并且以逗号分割。


##### 请求示例

请求body
```
{
    "dryrun":false,
    "names":[
    	"golang",
    	"private"
    ]
}

```
返回结果
```
{
    "items": [
        {
            "name": "golang",
            "template": {
                "kind": "ClusterPipelineTemplate",
                "name": "GoLangBuilder.2.6.0",
                "namespace": ""
            },
            "previousversion": "2.6.0",
            "version": "2.10.1",
            "status": "Failed",
            "message": "Validate pipelineconfig  Failed, Message is [error happened in the process of genereate jenkinsfile: multi errors, errors=ValidateError:UseNotification should be boolean, data=map[RecievedType:string RecievedValue:]\n] "
        },
        {
            "name": "private",
            "template": {
                "kind": "PipelineTemplate",
                "name": "GeneralBuild.1.9.12",
                "namespace": "devops"
            },
            "previousversion": "1.9.12",
            "version": "1.9.15",
            "status": "Succeed",
            "message": "Upgrade pipelineconfig  Succeed, Message is [] "
        }
    ]
}
```
在以下返回结果中，我们可以得到如下信息：

- 流水线名称
- 流水线对应模板
- 升级前后版本
- 升级结果状态
- 升级结果信息

并且根据message我们可以得到以下信息

- 名称为 golang 的流水线在进行数据校验的时候失败
- 名称为 private 的流水线更新成功



##### 检查是否生效

* 直接在页面检查流水线生成的 jenkinsfile 是否是使用新版本的模板生成

* 通过  `kubectl get pipelineconfig <流水线名称> -n <项目名称> -o yaml` 查看`spec.template.pipelineTemplateRef.name`字段的值是否自动变化为对应模板的最新版本，然后通过 `kubectl get pipelineconfig <流水线名称> -n <项目名称>` 确认流水线的状态是否变为 `Ready`. 


##### 注意事项

因为批量流水线升级不可回滚，因此建议在流水线批量升级时，遵循以下原则
* 一定要先 dryrun 检查数据
* 先小批量更新探路，再过渡到大批量更新

#### 可能的问题及风险

由于新版本的模板中，可能添加了新的参数或者逻辑，会导致升级到新版本后，出现不兼容的情况。 需要在升级后，到页面上，更新对应的流水线，补充填写需要的参数，再次保存。

#### 问题排查

- 更新接口不存在或者报错：检查使用的 devops-api 版本是否为对应版本

- 如何判断流水线是否真的进行了升级： 对流水线升级后，可以在集群中查看升级过的 pipelineconfig 的版本，即 label 中的 templateVersion，如果版本升级并且 piplineconfig 状态正常，则升级成功

- 如何判断流水线升级是在哪里出错的呢： 因为现在的处理逻辑，会包含 preview 和 upgrade 两个逻辑，在不同阶段的处理结果会同步到 API请求 的返回结果中，所以我们可以通过 API处理 的返回的 Message 来判断出错位置

- 返回正常，但是流水线没有实际更新怎么办： 发送 API请求后成功后，pipelineconfig的 资源已经被更新，但是生成 jenkinsfile 的操作，是由对应的 controller 来完成的。所以如果升级成功，但是流水线没有实际更新，我们需要查看 devops-controller 的状态和日志来判断情况

