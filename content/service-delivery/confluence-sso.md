+++
title = "文档管理工具 Confluence 支持单点登录"
description = ""
weight = 1
alwaysopen = false
+++

## 背景
目前需要提供一种可以通过ACP平台单点登录到 Confluence 产品的方式，方便用户使用


## 调研
### 信息
目前Atlassian没有为Confluence产品提供无偿的SSO方式，推荐的方式是使用Crowd，插件，自定义验证器或者通过jira进行托管，可参考 [Atlassian产品单点登录集成](https://confluence.atlassian.com/kb/single-sign-on-integration-with-atlassian-products-794495126.html)。

其中只有插件KSSO支持OIDC协议的单点登录。

目前ACP dex作为身份验证提供商，只支持OIDC协议

### 结论
如果需要通过 ACP平台 可以单点登录到 Confluence 产品，则需要使用插件产品 KSSO，详细内容可参见 [插件KSSO详情](https://marketplace.atlassian.com/apps/1212126/k-sso-saml-kerberos-oauth-for-confluence?hosting=server&tab=overview)，目前该插件为付费插件。

## 使用示例
### 前提
插件要求confluence支持https访问，而且如果ACP平台为自签名证书，则需要其他的额外处理。在本文档中，将会 **演示** 使用deployment部署的confluence如何进行设置，如果用户已经有了confluence服务，也可以参考文档中提到的官方文档进行修改。

----

注： 
该文档中提到的部署confluence的方式以及相应参数设置，仅为示例内容，其中做出的修改应该根据用户部署不同做相应的调整。

----

### 部署confluence

#### 配置https访问
因为插件要求redirectURL参数为https,所以需要为 confluence 配置 https 访问方式。并记下此时的域名。

如果用户已经有了部署的confluence，可以参考 [Running Confluence Over SSL or HTTPS](https://confluence.atlassian.com/doc/running-confluence-over-ssl-or-https-161203.html) 或者 [Running Confluence behind NGINX with SSL](https://confluence.atlassian.com/doc/running-confluence-behind-nginx-with-ssl-858772080.html) 将confluence改为https访问。

#### 自签名证书处理方式
如果ACP平台使用的是自签名证书，需要额外设置 truststore, 首先将ACP平台公钥导出为public.crt，导出方式参见 [https网站如何导出公钥](https://blog.csdn.net/lllkey/article/details/17526323), 或者在acp的global的master 节点执行`cat /etc/kubernetes/manifests/kube-apiserver.yaml |grep oidc-ca-file`将其拷贝为public.crt。

导出后，使用keytool工具将该文件转换成cacerts并保存
```
keytool -import -alias int -keystore /path/to/your/cacerts -file public.crt
```

如果用户已经有了部署的confluence，可以参考 [How to import a public SSL certificate into a JVM](https://confluence.atlassian.com/kb/how-to-import-a-public-ssl-certificate-into-a-jvm-867025849.html) 进行设置

#### 通过 k8s 部署 confluence
按照对应参数填写部署deployment，部署并进行设置

其中需要修改

* volumes.cacerts.hostPath.path为你刚才生成cacerts的路径
* evn.ATL_PROXY_NAME 为https对应的域名


注意,在初始化设置的时候，使用IP访问confluence,设置结束后,再换为域名访问
```   
   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: confluence
     labels:
       app: zpyuconfluence
   spec:
     replicas: 1
     selector:
       matchLabels:
         app: zpyuconfluence
     template:
       metadata:
         labels:
           app: zpyuconfluence
       spec:
         nodeSelector:
           ip: 192.168.xx.xx
         containers:
         - name: confluence
           image: atlassian/confluence-server:6.13.0-alpine
           imagePullPolicy: IfNotPresent
           volumeMounts:
           - mountPath: /var/atlassian/application-data/confluence
             name: data
           - name: cacerts
             mountPath: /var/atlassian/application-data/confluence/cacerts
           env:
           - name: ATL_PROXY_NAME
             value: "xx.alauda.cn"
           - name: ATL_PROXY_PORT
             value: "443"
           - name: ATL_TOMCAT_SCHEME
             value: "https"
           - name: ATL_TOMCAT_SECURE
             value: "true"
           - name: JVM_SUPPORT_RECOMMENDED_ARGS
             value: "-Djavax.net.ssl.trustStore=/var/atlassian/application-data/confluence/cacerts"
         volumes:
          - hostPath:
              path: /root/confluence/data
              type: DirectoryOrCreate
            name: data
         - name: cacerts
           hostPath:
             path: /root/confluence/cacerts
             type: File
   -----
   kind: Service
   apiVersion: v1
   metadata:
     name: confluence
   spec:
     type: NodePort
     selector:
       app: zpyuconfluence
     ports:
     - protocol: TCP
       port: 8090
       targetPort: 8090
       nodePort: 30000
```

进入 管理页面 - 用户组，创建用户组，在 管理 - 全局权限 中，为该用户组授予相关权限，该组的权限将会赋予通过SSO登录进来的用户。记下此时组名，在之后插件设置中会用到

### 插件安装
以管理员身份登录confluence，进入 站点管理/一般配置 - 查找新应用 - 搜索ksso - 安装

安装成功后，进入 站点管理/一般配置 - 用户&安全 - Kantega SSO Enterprise， 进行provider的添加

- 选择 SAML and OpenID Connect
- provider 选择 Other
- Select protocol for SSO integration 选择 OpenID Connect
- 勾选 Create accounts on-the-fly for non-existing users when they login  && 填写我们在上一步中加入的组名
- 确保 Callback URL 是https协议,并且记录此时的 Callback URL，该数据在后面需要用到
- 根据提示填写identity server，一般该地址为 平台域名/dex 比如：int.alauda.cn/dex
- 填写公司名称(任意，无限制，会在登录的时候作为登录选项)
- 填写Credentials中Client ID和Client Secret，一般该值在 dex-configmap 中，可以通过 kubectl get configmap dex-configmap -n cpaas-system 获得。Client ID和Client Secret 值类似于 alauda-xxxx 和 ZXhhbXBsZS1hcHxxxxxxxxxx
- 将上述设置中得到的Callback URL写到 dex-configmap 的 data.staticClients.redirectURIs 中(kubectl edit configmap dex-configmap -n xxxxx-system),操作后,重启dex.
- 结束

### 测试登录
注销账号，登录的时候选择对应的provider，通过ACP可以跳转登录到confluence上即可

### 总结
以上演示了使用该插件需要额外设置的内容，主要包括以下两个方面，可以根据用户实际情况进行修改

- confluence的需要支持 HTTPS 协议进行访问
- 平台如果是自签名证书，则需要额外设置cacerts




