+++
title = "总体流程"
description = ""
weight = 1
alwaysopen = true
+++

![coderepo](/images/coderepo-flow.png)

`devops-controller` 中， `coderepobinding controller` 会 监听`devops-apiserver`, 如果有新的coderepobinding被创建或者被更新，或者满足了定期的时间， 都会对coderepobinding 资源进行同步。  
同步的时候， 会根据当前 coderepobinding的资源上配置的信息，例如绑定的账号是什么，仓库是什么，根据这些信息，请求工具的API， 获得对应的仓库列表。  
将相应的仓库列表转换为 `coderepository` 资源，存储在集群中，与当前的binding的namespace相同。   