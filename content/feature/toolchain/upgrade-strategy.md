+++
title = "工具链升级策略"
description = ""
weight = 60
alwaysopen = true
+++

|工具|文档| 说明|
|-----|----|----|
| `Jenkins` | [Jenkins升级策略文档](/components/alauda-jenkins/upgrade-strategy.md)| |
| `Harbor` | [Harbor升级策略文档](harbor-upgrade-strategy.md)| |