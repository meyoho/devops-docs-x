+++
title = "工具链"
description = ""
weight = 2
alwaysopen = true
+++

#### 工具的类别

目前支持的工具可以查看平台手册获取。工具的类别在 k8s集群中由资源Kind 来标识。  
例如，查看所有代码仓库类型的工具：  
```
[root@devops-master1 ~]# kubectl get codereposervice
NAME                TYPE        HOST                          STATUS   AGE
bitbucket           Bitbucket   https://api.bitbucket.org     Ready    27d
gitea               Gitea       http://10.0.0.0:32030/   Ready    23d
gitee               Gitee       https://gitee.com             Ready    27d
github              Github      https://api.github.com        Ready    27d
gitlab              Gitlab      https://gitlab.com            Ready    27d
```
使用 type 来标识特定的工具。  
可以查看资源的 status 字段，判断当前工具的基本的健康状态。  
```bash
[root@devops-master1 ~]# kubectl get codereposervice gitlab -o yaml
apiVersion: devops.alauda.io/v1alpha1
kind: CodeRepoService
metadata:
  name: gitlab
spec:
  ...
status:
  conditions:
  - lastAttempt: "2020-01-19T09:39:23Z"
    name: gitlab
    status: Ready
    type: HTTPStatus
  http:
    delay: 1244557878
    errorMessage: ""
    lastAttempt: "2020-01-19T09:39:23Z"
    statusCode: 200
  lastUpdated: null
  phase: Ready
```

####  工具的集成

工具的集成过程，实际上就是创建一个对应类型的工具资源。 可以通过如下命令查看系统中的工具类型  
```bash
kubectl get tooltype
```

例如，要创建一个 gitlab的集成， 则可以使用以下 yaml:
```yaml
# gitlab-demo-1.yaml
apiVersion: devops.alauda.io/v1alpha1
kind: CodeRepoService
metadata:
  name: gitlab-demo-1
spec:
  http:
    accessUrl: https://your-gitlab-address.com
    host: https://your-gitlab-address.com
  type: Gitlab
```
执行命令 `kubectl create -f gitlab-demo-1.yaml` 即可

####  工具的绑定

工具的绑定过程，实际上就是创建一个对应类型的绑定资源。 可以通过资源类型，查看当前拥有的绑定资源， 例如查询代码仓库的绑定。 
```bash
kubectl get coderepobinding --all-namespaces
```

例如，要创建一个 gitlab的绑定， 则可以使用以下 yaml
```yaml
# gitlab-demo-binding.yaml

apiVersion: devops.alauda.io/v1alpha1
kind: CodeRepoBinding
metadata:
  name: demo
  namespace: demo
spec:
  account:
    owners:
    - all: true
      name: development
      repositories: []
      type: Org
    secret:
      name: gitlab-demo
      namespace: demo
  codeRepoService:
    name: gitlab
```
执行命令 `kubectl create -f gitlab-demo-binding.yaml` 即可




