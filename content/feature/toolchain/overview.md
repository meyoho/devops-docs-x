+++
title = "总体流程"
description = ""
weight = 1
alwaysopen = true
+++

![toolchain](/images/toolchain-flow.png)

![toolchain](/images/toolchain-flow.2.png)

####  平台工具集成

用户通过平台的API或页面功能，创建平台的集成实例。这些集成实例属于平台级别的资源,将会存储在 k8s中。  
devops-controller中的工具集成的controller, 会监听相应工具的事件，对集成实例的状态进行同步（Reconcile）。 例如，当前工具访问是否正常等状态进行定期的检查。  
将最终的同步结果保存在资源的 condition中，并通过status和message反馈当前资源的状态。  
这些资源的状态，最终会在平台界面上显示。

#### 工具绑定

用户通过平台绑定的API(或页面功能)，输入工具的凭据等信息，完成工具实例在某个项目下的绑定。这些绑定的资源，都会属于某一个项目，在k8s中，也就是一个namespace级别的资源。  
`devops-controller`中， 各个绑定的controller， 会监听相应的绑定的事件，对绑定资源的状态进行同步（Reconcile）。例如， 当前工具访问是否正常，使用的凭据是否正确，凭据是否存在等等。  
将最终的同步结果保存在资源的 condition中，并通过status和message反馈当前资源的状态。 
同时，这些绑定的资源，还会根据用户绑定的工具信息，生成相应的工具内的资源。例如，代码仓库的绑定会生成具体的代码仓库。  