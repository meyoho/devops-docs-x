+++
title = "总体流程"
description = ""
weight = 1
alwaysopen = true
+++

流水线的管理操作
![pipeline-created](/images/pipeline-created.png)

读取日志的流程
![pipeline-log](/images/pipeline-log.png)

在Jenkins 上会有 AlaudaDevOpsSync 插件， 负责监听 k8s 和jenkins 事件，进行数据的同步。