+++
title = "脚本流水线"
description = ""
weight = 3
alwaysopen = true
+++

脚本内容将会保存在 PipelineConfig上。

- spec.strategy.jenkins.jenkinsfilePath: 保存jenkinsfile 在代码仓库中的路径
- spec.strategy.jenkins.jenkinsfile: 保存jenkinsfile 的 具体内容

```
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineConfig
metadata:
  name: junit
  namespace: what-ever
spec:
  jenkinsBinding:
    name: jenkins
  runPolicy: Serial
  source:
    git:
      ref: master
      uri: https://gitee.com/devops/what-ever
    sourceType: GIT
  strategy:
    jenkins:
      jenkinsfilePath: Jenkinsfile-junit-k8s
```