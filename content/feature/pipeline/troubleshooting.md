+++
title = "常见问题排查"
description = ""
weight = 5
alwaysopen = true
+++

### 流水线


#### jenkins健康检查

#### 图形化和模板流水线执行按钮为灰色，无法正常点击执行。

**解释**  
  图形化和模板的流水线，创建完成或者更新之后，都会先进行 jenkinsfile的生成操作。   
  完成 jenkinsfile 生成之后，由jenkins插件将流水线同步到 jenkins上。
  所以，在同步到 jenkins 上之前， 是无法执行的。  
  这个同步过程是异步的，但通常会很快。同步完成后，pipeline 资源的状态会 变为 ready 状态。这个时候才能正常执行。

**排查与解决**  
  首先确认当前流水线配置的状态，即 当前 pipelineconfig的 status.phase 字段, 参考 [查看当前资源的状态](/system/troubleshooting/#如何查看当前资源的状态)  

  - 如果 status.phase 字段的值为 creating 
    * 确认 `devops-controller` 是否运行正常。通过`kubectl get pods -n {namespace} | grep devops-controller` 查看pod 状态
      - pod 正常运行
          - 执行 `kubectl logs -f deploy/devops-controller -n {namespace} --tail 2000` ， 查看日志中是否有error 信息，通过error信息进行判断,或将其发给oncall排查。
      - pod 未启动
          - 参考 [devops-controller无法正常启动](/system/troubleshooting/#devops-controller无法正常启动)
  - 如果 status.phase 字段的值为 syncing
    * 确认jenkins是否工作正常, 参考 [jenkins健康检查](#jenkins健康检查)
      - 正常, 走 oncall流程解决。
      - 不正常, 按照 健康检查内的提示进行修复， 保证jenkins处于健康状态。 然后重新触发，进行验证。
  - 如果 status.phase 字段的值为 error
    * 查看 `status.message` 字段的内容
      - 内容为 `Render Jenkinsfile Error`, 参考[Render Jenkinsfile Error](#流水线处于错误状态-错误信息提示-render-jenkinsfile-error)
      - 内容为 `convert job error`， 如果 reason 中提示 `happened in the process of genereate jenkinsfile`, 则参考 [Render Jenkinsfile Error](#流水线处于错误状态-错误信息提示-render-jenkinsfile-error)

**相关信息**


#### 流水线处于错误状态，错误信息提示 `Render Jenkinsfile Error`

**解释**  
  图形化和模板的流水线，创建完成或者更新之后，都会先进行 jenkinsfile的生成操作。 可能由于模板本身有问题，导致jenkinsfile 渲染出错。  

**排查与解决**  
  首先确认当前使用模板是官方模板还是自定义模板  

  - 官方模板
    - 确认各个组件版本是否正确，如果正确，则根据 [查看当前资源的状态](/system/troubleshooting/#如何查看当前资源的状态)， 获取资源的 status 信息， 发给oncall， 走oncall 流程。
  - 自定义模板
    - 大概率是自定义模板本身有问题，根据 [查看当前资源的状态](/system/troubleshooting/#如何查看当前资源的状态)， 获取资源的 status 信息， 发给oncall， 走oncall 流程。  

**相关信息**

#### 流水线处于错误状态，错误信息提示 "cannot convert pipelineconfig to job"

TODO

#### 脚本和多分支流水线执行按钮为灰色，无法正常点击执行。

**解释**  
  jenkins插件需要将流水线同步到 jenkins上， 在同步到 jenkins 上之前， 是无法执行的。  
  这个同步过程是异步的，但通常会很快。同步完成后，pipelineconfig 资源的状态会 变为 ready 状态。这个时候才能正常执行。

**排查与解决**
  确认jenkins是否工作正常, 参考 [jenkins健康检查](#jenkins健康检查)
  
  - 正常, 走 oncall流程解决。
  - 不正常, 按照 健康检查内的提示进行修复， 保证jenkins处于健康状态。 然后重新触发，进行验证。

#### 多分支流水线扫描按钮为灰色，无法点击扫描。

**解释**  
  jenkins插件需要将流水线同步到 jenkins上， 在同步到 jenkins 上之前， 是执行扫描的。  
  这个同步过程是异步的，但通常会很快。同步完成后，pipelineconfig 资源的状态会 变为 ready 状态。这个时候才能正常执行。

**排查与解决**
  确认jenkins是否工作正常, 参考 [jenkins健康检查](#jenkins健康检查)
  
  - 正常, 走 oncall流程解决。
  - 不正常, 按照 健康检查内的提示进行修复， 保证jenkins处于健康状态。 然后重新触发，进行验证。

#### 流水线触发执行后，提示触发成功，但流水线记录一直处于已触发状态

#### 流水线一直处于排队中，无法正常进入执行状态


### 流水线-代码扫描

#### 启用多分支扫描非master分支的代码， 流水线代码扫描失败

1. 当启用多分支扫描时，需要先扫描 master的分支之后，才能正常扫描其他分支。

### 流水线-更新应用

### 流水线-构建镜像
