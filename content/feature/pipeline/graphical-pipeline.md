+++
title = "图形化流水线"
description = ""
weight = 3
alwaysopen = true
+++


PipelineConfig 中会保存图形化的信息， 该类型的流水线保存后，会临时生成一个模板， 会使用该模板进行jenkinsfile的生成。

![pipeline-grahp-create](/images/pipeline-grahp-create.png)

图形化的信息将会保存在 `PipelineTemplateMold` 中。

```go
// PipelineTemplateMold  是  PipelineTemplate 类型的子集
type PipelineTemplateMold struct {
  // +optional
  metav1.ObjectMeta `json:"metadata"`
  Spec              PipelineTemplateSpec `json:"spec"`
}
```

后端接收到 前端的 数据后， 根据 `PipelineTemplateMold` 信息自动生成一个临时的流水线模板。
模板的名称为 `{PipelineConfigname}-template-{MoldHash}`
模板创建后，则使用该模板进行渲染生成 Jenkinsfile.

通过 `kubectl get pipelineconfig` 可以看到生成的模板： 
![graph-pipelineconfig](/images/graph-pipelineconfig.png)

含有名为 `templateName` 来记录生成的模板名称
![graph-pipelineconfig](/images/graph-pipelineconfig-labels.png)