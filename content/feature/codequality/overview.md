+++
title = "总体流程"
description = ""
weight = 1
alwaysopen = true
+++

![codequality-flow](/images/codequality-flow.jpg)

在`devops-controller`中，`codequalitybinding controller` 会监听 `devops-apiserver`,  
当前有新的资源创建或更新，或者满足定期时间时， 会进行同步。  
通过访问 对应工具的API，检查目标工具的项目列表，和本地资源做匹配，进行 `CodeQualityProject`的创建，更新或删除操作。  
最终，同时将对应的扫描报告同步到平台上。