+++
title = "代码质量"
description = ""
weight = 100
alwaysopen = false
+++


#### 扫描项目以及报告的生成

当一个 `CodeQualityToolBinding` 资源被创建后，devops-controller会每隔一段时间，对该资源进行同步。  
同步的过程中，会调用sonarqube的API， 获取当前工具内的所有的扫描项目。 并且将该扫描项目的id 和 当前租户下的代码仓库的id 做对比。
如果， 该sonarqube 的项目id 对应的代码仓库存在， 则认为是当前项目的扫描项目，生成`CodeQualityProject`资源。  

生成 `CodeQualityProject`资源后， 每个 `CodeQualityProject`资源会定期的发生 reconcile， 从sonarqube 上获取对应的扫描结果，保存在当前资源上。
前端即可请求获得对应的信息。


#### 在流水线中的使用

在流水线中，可以使用代码扫描的 任务，执行代码扫描。  
请求 `CodeQualityBinding` 的 资源列表，获得 当前项目下的工具绑定。  
在执行扫描的时候， 调用`sonar-scanner`, 执行扫描命令， 执行扫描前，会根据当前代码仓库的地址， 换算为特定名称的扫描项目ID，设置到 sonar-properties中，确保最终生成的扫描项目的ID符合预期。  
如果用户勾选了等待扫描完成，则会在流水线中，访问sonarqube的API,不断检查对应的扫描结果是否通过。  
如果不通过，则将流水线置位失败。