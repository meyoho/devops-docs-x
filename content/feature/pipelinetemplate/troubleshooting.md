
+++
title = "常见问题排查"
description = ""
weight = 5
alwaysopen = true
+++

#### 官方模板导入后，在创建流水线的时候发现选不到

**解释**

  首先需要了解官方流水线的导入的方式，参考 [官方模板导入](/feature/pipelinetemplate/default/#模板导入)

**排查与解决**
  
  - 首先确认 devops-controller 中 initContainers 中的容器为正确的模板镜像版本
  
  - 确认devops-controller的状态正常启动
   
  - 可以通过如下命令， 查看官方模板的导入结果：
      ```bash
      kubectl get clusterpipelinetemplatesync TemplateSyncOfficial -o yaml 
      ```

  - 根据导入结果判断发生的具体问题
  
  - 上述方法不能解决问题，则需要查看 devops-controller 相关的日志

**相关信息**

#### 导入模板后，发现流水线没有对应的升级按钮

**解释**

  流水线可以展示升级按钮，需要满足以下条件
  
  - 流水线对应的模板的版本有改变，并且版本号增加
  
**排查与解决**

  首先需要检查导入的模板版本号是否发生了改变，并且需要确认版本号是增加的
  
  检查模板是否存在，参考 [模板的版本管理](/feature/pipelinetemplate/default/#模板的版本管理)
  
  如果模板不存在，则需要检查模板导入是否成功，查看对应资源的condition来确定问题，参考 [官方模板导入](/feature/pipelinetemplate/default/#模板导入)
  
