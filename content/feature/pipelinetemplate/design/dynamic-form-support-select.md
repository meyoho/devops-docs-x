+++
title =  "动态表单支持下拉框选择"
date = 2020-02-27T22:12:50+08:00
tags = []
featured_image = ""
description = ""
+++

# 背景

JIRA http://jira.alauda.cn/browse/REQ-1695

需求总结：
- 需要支持在模板编写时指定可选的下拉内容
- 展示的内容和提交的内容可能不同
- 下拉选择的同时也可以输入

# 目标

- 需要支持在模板编写时指定可选的下拉内容
- 展示的内容和提交的内容可能不同
- 下拉选择的同时也可以输入,考虑将来扩展，允许约束在某个范围内

为了保持模板编写的门槛， 不考虑支持更复杂的，动态获取第三方下拉数据的方式。

# 方案内容

## 整体思路

- 增加新的动态表单展示类型select，定义新的 schema, 模板中枚举可选项，前端下拉展示。
- 增加string 类型字段的 辅助输入选项。

## Model 设计

- 下拉框

*display.type*: select
*schema.type*: string
*schema.enum*: 字符串数组，表示值的选择范围，后端校验。
*display.enumAlias*: 值的别名， 前端下拉展示使用，使用索引和 schema.enum 对照。可选，如果没有enumAlias，则前端使用 schema.enum 来进行下拉。

模板样例

```
spec:
  arguments:
  - name: "issueType"
    schema:
      type: string
      enum:   #表示值的选择范围
      - "BUG"
      - "IMP"
      - "REQ"
    display:
      type: "select" # 下拉框
      name:
        zh-CN: 构建环境
        en: agent
      enumAlias: # 可选， 和enum 按照数组的索引对应，表示下拉时展示的内容。
      - "缺陷"
      - "优化"
      - "需求"
    required: true
    default: "BUG”
```

- 辅助输入

*display.suggestions*: 字符串数组，当display.type=string 时，通过display.suggestions 来展示辅助输入内容。

模板样例

```
spec:
  arguments:
  - name: "issueType"
    schema:
      type: string
    display:
      type: "string"
      name:
        zh-CN: 构建环境
        en: agent
      suggestions:   # 供前端辅助输入使用,提交的值即为选择的值
      - "bug"
      - "req"
      - "imp"
    required: true
    default: "bug"
```

后端数据结构变化

```
type ArgItemSchema struct {
	Type  string             `json:"type,omitempty"`
	...

	// 新增加的 字段
	Enum []string `json:"enum,omitempty"`
}

type ArgDisplayInfo struct {
	Type        string                 `json:"type"`
  // ...


	EnumAlias   []string               `json:"enumAlias,omitempty"`
	Suggestions []string               `json:"suggestions,omitempty"`
}
```

## API 设计

无

## 权限设计

无

## 功能开关

无

### 涉及组件

- jenkinsfilext: 增加新的数据结构支持及校验
- devops-apiserver: jenkinsfilext 依赖更新

### 数据兼容

无

### 部署相关

无

### 文档交付

- 动态表单的说明文档

# 遗留问题和缺陷

无

# 其他

无