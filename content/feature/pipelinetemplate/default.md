+++
title = "流水线模板"
description = ""
weight = 2
alwaysopen = true
+++

#### 模板的分类

根据 K8S的资源类型来划分， 分为以下类别：

- `TaskTemplate`: Namespaced 级别的 任务模板，只允许项目下使用。
- `PipelineTemplate`: Namespaced 级别的 流水线模板，只允许项目下使用。
- `ClusterTaskTemplate`: 平台级别的 任务模板，允许所有项目使用。
- `ClusterPipelineTemplate`: 平台级别的 流水线模板， 允许所有项目使用。

其中， `ClusterPipelineTemplate` 和 `PipelineTemplate` 结构类似， `ClusterPipelineTaskTemplate` 和 `PipelineTaskTemplate` 结构类似。
模板的写法和数据结构可以参考 [pipelinetemplate](/components/pipeline-templates/)。

也就是说，可以通过命令 `kubectl get tasktemplate -n {namespace}` 来获取集群内存储的资源。对于 Cluster级别的资源，属于所有 namespace 共有。

按照来源， 分为一下类别  

- `official`: 官方模板
- `customer`： 客户自定义模板

会使用模板的 label 进行标识。 例如：

```
kind: ClusterPipelineTemplate
metadata:
  name: GolangBuild
  labels:
    source=official

---
kind: ClusterPipelineTemplate
metadata:
  name: GolangBuild
  labels:
    source=customer
```

在业务定义上， 官方模板， 一定是 Cluster 级别的，客户的自定义模板，有可能是  Cluster级别的，也有可能是namespace 级别的。

使用图表来表示

| \ | `source=official` | `source=customer` |
|-----|----|---|
| Cluster| 官方模板|平台级别自定义模板|
| Namespaced| 无 |项目级别的自定义模板|

目前，产品上并没有将 `自定义的平台级别的模板` 产品化，也就是说，没有相应的入口。

#### 模板导入

可以将流水线模板 从 代码仓库中，导入到 平台中。在代码仓库中实现版本管理， 导入到平台后，供普通用户使用。

**项目级别的模板导入**

项目级别的模板导入后，属于当前项目私有, 导入请求由资源 `PipelineTemplateSync` 来实现。
devops-controller 会根据资源的定义， 拉取代码， 读取模板文件，导入到平台中。

例如：
``` yaml
# templatesync.yaml
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineTemplateSync
metadata:
  name: TemplateSync
  namespace: xxxxxx
spec:
  source:
    # 绑定的代码仓库
    codeRepository:
      name: bitbucket-mathildetech-templates
      ref: master
    # 手动输入的代码仓库地址
    git:
      ref: master
      uri: https://bitbucket.org/mathildetech/xxxxx.git
    # 仓库对应的凭据
    secret:
      name: devops-bitbucket
      namespace: global-credentials
    sourceType: "GIT"
```

使用命令 `kubectl create -f templatesync.yaml` , 则可以在某个项目下手动创建一个导入请求。
通过 `kubectl get PipelineTemplateSync TemplateSync -o yaml`, 查看 yaml 中 的 condition， 查看不同模板的导入是否成功。  
例如：
```
[root@devops-master1 ~]# kubectl get pipelinetemplatesync -n xxxx TemplateSync -o yaml
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineTemplateSync
metadata:
  name: TemplateSync
  namespace: xxxx
spec:
  ...
status:
  commitID: 89889b44b95ec7225eae1c6457bae6f5b7c7ea5c # 表示当前导入的代码仓库的版本
  conditions:
  - lastTransitionTime: "2020-01-14T02:27:57Z"
    lastUpdateTime: "2020-01-14T02:27:57Z"
    message: Skiped
    name: alauda-all-in-one
    previousVersion: 2.6.1
    reason: ""
    status: Skip  # 表示 名为 alauda-all-in-one 的 task 模板被跳过了， 版本都是2.6.1, 文件的名称是 task-all-in-one.yaml
    target: /tasks/task-all-in-one.yaml
    type: PipelineTaskTemplate
    version: 2.6.1
```

**平台级别的模板导入**

目前该功能只是内部体验功能， 并没有提供对应的产品化功能.  
平台级别的模板导入之后， 所有项目下都会看到。具体可以参考产品手册。  

进行平台级别的模板导入时， 会生成 `ClusterPipelineTemplateSync` 的资源，devops-controller 会根据资源的定义， 拉取代码， 读取模板文件，导入到平台中。

例如：
``` yaml
# templatesync.yaml
apiVersion: devops.alauda.io/v1alpha1
kind: ClusterPipelineTemplateSync
metadata:
  name: TemplateSync
spec:
  source:
    # 手动输入的代码仓库地址
    git:
      ref: master
      uri: https://alaudabot@bitbucket.org/mathildetech/xxxx.git
    # 仓库对应的凭据
    secret:
      name: devops-bitbucket
      namespace: global-credentials
    sourceType: "GIT"
```
使用命令 `kubectl create -f templatesync.yaml` , 则可以创建一个全局的模板导入请求。

**官方模板导入**

官方模板导入是系统启动后，自动导入的， 不需要， 也不要手动再进行该项操作。  
在 devops-controller 启动后，会自动创建一个 `clusterpipelinetemplatesync/TemplateSyncOfficial` 的资源，devops-controller 会根据资源的定义， 从容器的存储中， 读取模板文件，导入到平台中，成为官方模板。

可以通过如下命令， 查看官方模板的导入结果：
```bash
kubectl get clusterpipelinetemplatesync TemplateSyncOfficial -o yaml 
```
查看导入的详细信息。 Conditions 中保存的有 所有模板的导入结果。

#### 模板的版本管理

为了保证， 当模板同步，导致某个模板被升级后，不影响 已经使用该模板的流水线的逻辑， 我们提供了保留模板历史版本的功能。  
通过 `kubectl get clusterpipelinetemplate` 可以看到  
```bash
[root@devops-master1 ~]# kubectl get clusterpipelinetemplates
NAME                                     DISPLAY NAME                            VERSION   CATEGORY        STATUS   AGE
GoLangAndDeployService                   Golang Build & Deploy Application       2.6.5     Build           -        27d
GoLangAndDeployService.2.5.0             Golang Build & Update Application       2.5.0     Build           -        27d
GoLangAndDeployService.2.6.5             Golang Build & Deploy Application       2.6.5     Build           -        17d
```
其中， `GoLangAndDeployService.2.5.0 `, `GoLangAndDeployService.2.6.5` 均为冗余的历史版本，`GoLangAndDeployService` 为最新版本。  
在页面上看到的模板，都是 最新版本。  
当创建流水线的时候， 会使用最新版本对应的冗余版本进行流水线的创建。 
也就是说，假如， 使用`GoLangAndDeployService`来创建流水线， 则实际最终创建出来的流水线，使用的模板是`GoLangAndDeployService.2.6.5`.  
当模板升级时， 只会升级 最新版本，冗余的历史版本内容将保持不变。  
可以参考版本管理的逻辑关系图：
![pipeline-versioned-template](/images/pipeline-versioned-template.png)

#### 模板的删除清理

对于不再使用的历史的流水线模板，会随着时间，自动被清理掉。

#### 模板的过滤条件

模板上有两种 label, `category`, `lang`。这两个 label的值 会在 页面上展示， 提供给用户进行模板的筛选。   

通过 API `devops/api/v1/pipelinetemplatecategories/{namespaces}` 获取相应的过滤条件，前端进行展示。  
前端请求对应的API 后， devops-api 会根据过滤条件进行筛选。

设计文档参考 http://confluence.alauda.cn/pages/viewpage.action?pageId=67437031


