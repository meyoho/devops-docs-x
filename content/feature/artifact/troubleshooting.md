+++
title = "常见问题排查"
description = ""
weight = 5
alwaysopen = true
+++

### Harbor

#### 打开harbor的ui页面后，选择镜像tag页面后，报错500，internal server error
![list tag error](/images/imageregistry/harbor-tag-error.png)

**解释**

harbor内部包含了一个原生的docker registry的组件，最终推送到harbor的镜像还是存储在这个registry上，但是harbor代理了这些外部请求，并在自己的数据库中维护了project及用户权限相关的关系，当调用api列出tag时，harbor会去请求registry，这个时候需要经过token的认证，假设此时registry组件的系统时间不对，假设比标准时间慢，此时registry在认证时，会认为签发token时间早于自己的系统时间，是非法的，所以拒绝了服务。

**排查与解决**

首先确认harbor的各个组件的pod是否运行正常，特别是core与registry这个两个pod，如果都正常，此时通过`kubectl get po -o wide`来找出registry这个pod所运行的node，使用ssh登录到这台node上，使用系统命令`date`来查看系统时间是否和标准时间有偏差，如果有偏差，调整系统时间后，再次回到harbor页面刷新页面尝试查看问题是否恢复，若未恢复，尝试重启registry组件，若仍未解决，可查看core组件与registry的日志排查是否有相关的error信息。

**相关信息**

#### harbor集成到平台后，工具有感叹号显示'username or password in secret xxx is incorrect'，并且有时恢复正常，有时又出现。

**解释**

目前在平台集成某个工具后，devops-controller会定期去同步这些工具的状态，如是否访问异常，密码是否正确等，此错误发生于调用工具的api鉴权时，工具侧返回的错误，需要排查工具侧的相关日志来定位问题。

**排查与解决**

- 前提步骤：
harbor通过用户名与密码basic认证，请确保用户名与密码是否正确，请尝试使用在平台集成的帐号密码在harbor的ui页面上尝试重新登录。

- 步骤一：
查看core的pod运行是否正常，若不正常尝试重启，然后通过在机器上通过curl直接访问harbor的api，查看返回情况，如果返回正常结果则多尝试几次观察结果，如下：
![curl api](/images/imageregistry/harbor-curl-api.png)

- 步骤二：
查看core组件的相关日志，排查是否有相关401报错的信息以及红色error信息，如'connection reset by peer的日志'，如下图：
![harbor-core-net-log](/images/imageregistry/harbor-core-net-log.png)
![harbor-core-401-log](/images/imageregistry/harbor-core-401-log.png)

- 步骤三：
如果是出现上图所示的error信息，则有可能是redis的pod与core的pod网络不稳定导致的，可尝试查看两个pod所运行的节点是否属于同一个，如果不是同一个，可以尝试通过nodeSelector将两个pod运行到同一节点后，并且重启harbor的所有组件的pod，再观察一段时间查看是否恢复，一般此情况都是由于网络不稳定导致的，和harbor本身及平台没有关系。

**相关信息**