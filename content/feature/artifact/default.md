+++
title = "镜像仓库"
description = ""
weight = 10
alwaysopen = false
+++

#### 镜像列表获取和TAG的获取

当一个 `ImageRegistryBinding` 资源被创建后，devops-controller会每隔一段时间，对该资源进行同步。  
同步的过程中，或调用对应工具的 镜像仓库API， 获取所有的镜像仓库，以及当前镜像仓库的tag列表。 

一个典型的镜像仓库资源示例如下：
```yaml
[root@devops-master1 ~]# kubectl get imagerepository -n xxxx harbor-test-imge -o yaml
apiVersion: devops.alauda.io/v1alpha1
kind: ImageRepository
metadata:
  name: harbor-test-imge
  namespace: xxxx
spec:
  image: test/imge
  imageRegistry:
    name: harbor
  imageRegistryBinding:
    name: harbor
status:
  conditions: null
  http:
    delay: 109959501
    errorMessage: ""
    lastAttempt: "2020-01-19T10:08:24Z"
    response: available
    statusCode: 200
  lastUpdated: "2020-01-19T10:08:25Z"
  latestTag:
    author: ""
    created_at: "2019-12-26T21:20:11Z"
    digest: edafc0a0fb057813850d1ba44014914ca02d671ae247107ca70c94db686e7de6
    level: 1
    message: ""
    name: latest
    scanStatus: finished
    size: 745.12KB
    summary: null
    updated_at: null
  phase: Ready
  tags:
  - author: ""
    created_at: "2019-12-26T21:20:11Z"
    digest: edafc0a0fb057813850d1ba44014914ca02d671ae247107ca70c94db686e7de6
    level: 1
    message: ""
    name: latest
    scanStatus: finished
    size: 745.12KB
    summary: null
    updated_at: null
```

#### 镜像扫描

镜像扫描功能调用 harbor 扫描API实现


#### 参考  

- http://confluence.alauda.cn/pages/viewpage.action?pageId=35684468