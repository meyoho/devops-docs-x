+++
title =  "Jfrog-Artifactory集成"
date = 2020-05-08T01:12:50+08:00
tags = []
featured_image = ""
description = ""
+++

# 背景

Jira：http://jira.alauda.cn/browse/DEVOPS-4238

目前光大，中信等金融客户均采购了Jfrog-artifactory作为自己的制品仓库，希望能够在产品中能够集成，Jfrog产品类似之前已集成的Nexus，不过在功能上更加丰富强大。

# 目标

在devops产品中提供集成Jfrog artifactory的能力，在产品形态上，功能类似已存在的nexus。

# 调研

目前这几个使用Jfrog的客户均采用的是6.X版本，并且目前社区积极维护的也是6.X与7.X版本，所以下面以为截至调研前的6.X的最新版本jfrog-artifactory-pro:6.19.1进行调研。

以下调研暂且只针对Jfrog下创建Docker镜像仓库，jfrog本身还支持创建多种仓库。

## 安装

```bash
docker run --name artifactory -d -p 8081:8081 docker.bintray.io/jfrog/artifactory-pro:6.19.1
```
然后通过浏览器访问ui即可：http://ip:8081，jfrog默认的帐号密码为admin/password，第一次登录时会要求修改默认密码以及输入产品授权码，产品授权码可去官网通过邮箱申请，授权码可使用1个月。

## 功能预览

在jfrog中，制品仓库又可分为四种类型：

   - Local：本地仓库，一般作为内部使用
   - Remote：远程仓库，一般作为代理并缓存远端仓库
   - Virtual：虚拟仓库，用于组织本地仓库和管理远程仓库
   - Distribution：发布仓库


![](/images/artifact/jfrog-repo-type.png)
选择一种类型后，就可以在此类型的仓库下创建不同类型的制品仓库（docker/pypi/npm...etc...）
![](/images/artifact/jfrog-package-type.png)
当选择镜像仓库后，按照基础配置只需要填写一个RepositoryKey就可以了，key很重要，作为仓库的唯一标识, 可以理解成仓库名
![](/images/artifact/jfrog-new-repo.png)
完成之后，就可以使用该镜像仓库了，此镜像仓库功能与原生的docker registry功能一致。不过在使用时，要注意以下的限制条件：

在jfrog系统http配置中，对于docker access提供了三种不同的访问方式，默认为Repository path，这三种方式影响的是push/pull镜像时所使用的格式。

- Port

           使用port方式访问时，在新建docker仓库时需要指定docker registry的binding port，对于每个镜像仓库都会使用一个不同的端口来区分不同的repository

           镜像格式：

                    docker pull / push 192.168.26.2:<REPOSITORY_PORT>/<IMAGE>:<TAG>
                    docker login -u <USER_NAME> -p <USER_PASSWORD> 192.168.26.2:<REPOSITORY_PORT>

- Repository Path(default)

           使用path方式时，不需要占用和配置额外的registry端口，而是通过url的path来区分

           镜像格式：

                    docker pull / push 192.168.26.2:8081/<REPOSITORY_KEY>/<IMAGE>:<TAG>
                    docker login -u <USER_NAME> -p <USER_PASSWORD> 192.168.26.2:8081

- Sub domain

          使用sub domain时，需要配置server name expression

          镜像格式：

                   docker pull / push <REPOSITORY_KEY>.192.168.26.2/<IMAGE>:<TAG>
                   docker login -u <USER_NAME> -p <USER_PASSWORD> <REPOSITORY_KEY>.192.168.26.2


# 方案内容

## 整体思路

整体的集成与分配流程上与nexus是一致的，最大的变化是通过jfrog集成镜像仓库时，不会创建docker类型的artifactregistry，而是以现有的imageregistry取代(会对由制品仓库集成的imageregistry添加相关label)。

## Model 设计

复用现有的结构，无新增model

## API 设计

集成jfrog相关api：

```json
// 集成 jfrog
// POST /api/v1/artifactregistrymanagers
// body:
{
    "metadata": {
        "name": "jfrog-xxxxxxxxxx",
        "annotations": {
            "cpaas.io/secretType": "kubernetes.io/basic-auth",
            "cpaas.io/toolType": "artifactRegistryManager"
        },
        "labels": {
            "cpaas.io/toolTypeName": "Jfrog-Artifactory"
        }
    },
    "spec": {
        "http": {
            "host": "http://1.1.1.1",
            "accessUrl": "http://1.1.1.1"
        },
        "secret": {
            "name": "zsy-github-secret",
            "namespace": "cpaas-system-global-credentials"
        },
        "type": "Jfrog-Artifactory"
    }
}
// response:
// 		status code: 200

// 更新 jfrog
// PUT /api/v1/artifactregistrymanagers/{name}
// body: 同上
// response: 同上

// 删除 jfrog
// DELETE /api/v1/artifactregistrymanagers/{name}
// response:
// 		status code: 200
```

jfrog详情页面

```json
// 列出该jfrog已经集成的docker仓库
// GET /api/v1/common/artifactregistrymanagers/{jfrog-name}/sub/artifactregistry?ArtifactType=docker
// response:
[
    {
        "apiVersion": "devops.alauda.io/v1alpha1",
        "kind": "ImageRegistry",
        "metadata": {
            "annotations": {
                "cpaas.io/createAppUrl": "http://ssss.com",
                "cpaas.io/subscription": "false",
                "cpaas.io/toolItemKind": "Docker",
                "cpaas.io/toolItemPublic": "false",
                "cpaas.io/toolItemType": "Docker",
                "cpaas.io/toolType": "artifactRepository"
            },
            "creationTimestamp": "2020-04-26T09:49:05Z",
            "labels": {
                "cpaas.io/subscription": "false",
                "cpaas.io/toolItemKind": "Docker",
                "cpaas.io/toolItemPublic": "false",
                "cpaas.io/toolItemType": "Docker",
                "cpaas.io/toolType": "artifactRepository"
            },
            "name": "docker-registry",
            "resourceVersion": "18269122",
            "selfLink": "/apis/devops.alauda.io/v1alpha1/imageregistries/docker-registry",
            "uid": "2a7cb878-87a3-11ea-8667-966ea4555622"
        },
        "spec": {
            "bindingPolicy": null,
            "data": null,
            "http": {
                "accessUrl": "",
                "host": "http://ssss.com"
            },
            "public": false,
            "secret": {},
            "type": "Docker"
        },
        "status": {
            "conditions": [
                {
                    "lastAttempt": "2020-05-08T05:49:30Z",
                    "name": "docker-registry",
                    "status": "Ready",
                    "type": "HTTPStatus"
                },
                {
                    "lastAttempt": "2020-05-08T05:49:31Z",
                    "name": "IndividualPolicy",
                    "status": "Ready",
                    "type": "ImageRegistry.IndividualPolicy"
                },
                {
                    "lastAttempt": "2020-05-08T05:49:31Z",
                    "name": "SharePolicy",
                    "status": "Ready",
                    "type": "ImageRegistry.SharePolicy"
                }
            ],
            "http": {
                "errorMessage": "",
                "lastAttempt": "2020-05-08T05:49:30Z",
                "statusCode": 200
            },
            "lastUpdated": null,
            "phase": "Ready"
        }
    }
]

```

jfrog详情页面新建仓库/集成已有仓库

```json
// 列出jfrog上的仓库并且过滤已经集成的仓库
// GET /api/v1/common/artifactregistrymanagers/{jfrog-name}/sub/repository?ArtifactType=docker&IsFilterAR=true
// response:
{
    "apiVersion": "devops.alauda.io/v1alpha1",
    "items": [
        {
            "metadata": {
                "creationTimestamp": null,
                "name": "cjtest",
                "selfLink": "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/cjtest/repository"
            },
            "spec": {
                "artifactRegistryArgs": {
                    "type": "local",
                    "key": "cjtest",
                    "url": "http://192.168.26.2:8081/artifactory/cjtest"
                },
                "artifactRegistryName": "cjtest",
                "http": {
                    "accessUrl": "",
                    "host": "http://10.0.128.241:32010/repository/cjtest"
                },
                "public": false,
                "secret": {},
                "type": "docker"
            },
            "status": {
                "conditions": null,
                "lastUpdated": null,
                "phase": ""
            }
        },
        {
            "metadata": {
                "creationTimestamp": null,
                "name": "cdcdc",
                "selfLink": "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/cdcdc/repository"
            },
            "spec": {
                "artifactRegistryArgs": {
                    "type": "local",
                    "key": "cdcdc",
                    "url": "http://192.168.26.2:8081/artifactory/cdcdc"
                },
                "artifactRegistryName": "cdcdc",
                "http": {
                    "accessUrl": "",
                    "host": "http://10.0.128.241:32010/repository/cdcdc"
                },
                "public": false,
                "secret": {},
                "type": "docker"
            },
            "status": {
                "conditions": null,
                "lastUpdated": null,
                "phase": ""
            }
        }
    ],
    "kind": "ArtifactRegistryList",
    "metadata": {
        "selfLink": "/apis/devops.alauda.io/v1alpha1/artifactregistrymanagers/nexus/repository"
    }
}

// 新建仓库
// POST /api/v1/common/artifactregistrymanagers/{jfrog-name}/sub/project
// body:
{
    "metadata": {
        "annotations": {
            "alauda.io/secretType": "kubernetes.io/basic-auth",
            "alauda.io/toolType": "artifactRegistry",
            "alauda.io/toolItemProject": ""
        }
    },
    "apiVersion": "devops.alauda.io/v1alpha1",
    "kind": "CreateProjectOptions",
    "secretname": "uiauto-nexussecret-global",
    "namespace": "global-credentials",
    "name": "xxxxxxxxxxxxxxxx",
    "isRemote": "false",
    "data": {
        "artifactRegistryName": "xxxxxxxxxxxxxxxx",
        "artifactType": "Docker",
        "type": "local"
    }
}
// response:
// 		status code: 200
```



## 权限设计

无

## 功能开关

新增devops-jfrog-artifactory功能开关，默认关闭

### 涉及组件

#### devops-apiserver
对于Jfrog创建出来的镜像仓库，由于和原生的docker registry除了在docker pull/push时有所差异外，对于原生的v2 api是兼容的，虽然可以考虑复用docker registry类型的imageregistry，不过由于考虑到后期jfrog的docker registry与原生的docker registry可能会有不兼容的地方，所以新增一个类型为jfrog-docker-registry的ToolType， 但是目前在行为上保持和集成一个docker registry的镜像仓库一致。

与之前集成nexus的区别在于之前的流程是 artifactregistrymanager ---> 创建/集成已有maven仓库 --->  创建artifactregistry资源，新的流程是 artifactregistrymanager ---> 创建/集成已有docker仓库 --->  创建imageregistry资源。镜像仓库本身也属于制品仓库的一种，所以没有必要专门针对docker类型的制品仓库在artifactregistry中专门实现一套针对docker仓库的逻辑(需要将imageregistry、imageregistrybinding的逻辑都同步一份)，直接复用imageregistry资源即可。



对于由artifactregistrymanager集成进来的docker仓库，需要在imageregistry资源中加上标识的label来方便后面的管理。已经设置ownerreferences为artifactregistrymanager以保证当jfrog被删除时，由jfrog集成的镜像仓库也被删除。

```yaml
apiVersion: devops.alauda.io/v1alpha1
kind: ImageRegistry
metadata:
  labels:
    ToolManager: "jfrog-name"
    ToolManagerKind: "ArtifactRegistryManager"
```

对于由jfrog创建的镜像仓库，访问docker registry的api示例如下：

镜像仓库test1: 

​	api地址： http://192.168.26.2:8081/artifactory/api/docker/test1

​	docker push/pull：docker pull 192.168.26.2:8081/test1/alaudak8s/devops-apiserver:latest

镜像仓库test2:

​	api地址： http://192.168.26.2:8081/artifactory/api/docker/test2

​	docker push/pull：docker pull 192.168.26.2:8081/test2/alaudak8s/devops-apiserver:latest

后端需要将集成的imageregistry的api地址，也就是http host设置为此镜像仓库的访问格式。http://192.168.26.2:8081/artifactory/（访问地址）+ /api/docker（固定路径）+ test1（仓库名称）


目前当使用Docker access method为默认方式 repository path时，对于生成的由于imageregistrybinding生成的repo，需要将annotation中的imageRegistryEndpoint调整为host+{/repoKey}，例如：192.168.26.2:8081/test1。这块考虑在admission的mutation中，针对由由jfrog集成的imageregistry，进行此annotation的处理。


#### devops-client(在devops-apisever仓库中 pkg/devops-client)
需要新增jfrog的客户端，根据目前使用到的jfrog的api编写swagger文档，再生成客户端。

目前主要需要使用以下两个api：

- list repositories：GET /api/repositories
- create repository: PUT /api/repositories/{repoKey}

#### ui
目前在管理视图工具链页面，toolchain接口会返回所有的imageregistry类型的数据，前端需要通过ToolManager的label来进行过滤，将由artifactRegistryManager集成进来的仓库显示在对应的artifactRegistryManager管理工具下。


### 数据兼容

无

### 部署相关

暂不提供chart。

### 运维排查

无

### 监控指标

无

### 文档交付

需要增加产品文档。

# 遗留问题和缺陷

- jfrog功能较为丰富，目前还未支持所有类型的仓库和包类型，也未支持镜像仓库的镜像扫描功能。

TODO improvement：

- 目前Nexus/Maven支持创建的仓库类型应该由后端返回，前端针对api结果做展示。
- 管理视图，工具链显示页面，目前通过/toolchain、/artifactregistrymanagers、/artifactregistry三个api返回结果，理想情况应该由后端聚合统一由/toolchain接口返回。

# 其他
无