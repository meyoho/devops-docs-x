+++
title =  "devops组件增加监控指标"
date = 2019-11-05T17:34:46+08:00
tags = []
featured_image = ""
description = ""
+++


## 组件运行监控指标
运行指标
http://confluence.alauda.cn/pages/viewpage.action?pageId=53936147

- CPU资源量(Requests)
- CPU资源量(Limits)
- 内存资源量(Requests)
- 内存资源量(Limits)
- Pod CPU使用率
- Pod 内存使用率
- Pod 网络接收速率
- Pod 网络发送速率

API指标
- API请求大小 Summary
- API响应大小 Summary 
- API请求消耗时间 Summary
- API请求次数 Counter
- API请求时间超长的请求统计 Gauge
- API 请求 Try Again 的次数 apiserver_dropped_requests Counter
- API Watcher 的个数 apiserver_registered_watchers Gauge

Controller指标
- reconcile 错误的次数在单位时间段内的统计。Histogram
- 不同资源的reconcile的处理时间。Guage 
- rest client 请求的时间延迟 rest_client_request_latency_seconds
- workqueue 中 正在处理的资源个数： adds_total{name=xx}  , client-go 已实现
- workqueue 中剩余待处理的个数： depth{name=xxx} ， client-go 已实现
- workqueue 中的平均等待时间：queue_latency_seconds{name=xxx}, client-go 已实现
- workqueue 中的平均处理时间：work_duration_seconds{name=xxx}, client-go 已实现
- workqueue 中的重试总次数：retries_total， client-go 已实现
- workqueue 中的长处理: longest_running_processor_microseconds , client-go已实现


## Controller 指标监控
现状
- controller-runtime 已经内置了 metrics server， 只需要实现一个 net.Listener.
- 注册到prometheus中

``` go
	// ReconcileTotal is a prometheus counter metrics which holds the total
	// number of reconciliations per controller. It has two labels. controller label refers
	// to the controller name and result label refers to the reconcile result i.e
	// success, error, requeue, requeue_after
	ReconcileTotal = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "controller_runtime_reconcile_total",
		Help: "Total number of reconciliations per controller",
	}, []string{"controller", "result"})

	// ReconcileErrors is a prometheus counter metrics which holds the total
	// number of errors from the Reconciler
	ReconcileErrors = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "controller_runtime_reconcile_errors_total",
		Help: "Total number of reconciliation errors per controller",
	}, []string{"controller"})

	// ReconcileTime is a prometheus metric which keeps track of the duration
	// of reconciliations
	ReconcileTime = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name: "controller_runtime_reconcile_time_seconds",
		Help: "Length of time per reconciliation per controller",
	}, []string{"controller"})
```

曾经遇到过的问题
- 需要处理的资源太多，将队列打满。 例如绑定了 alauda-registry镜像仓库， 会导致 tag一直同步不过来。
- 单个需要同步的资源，消耗的时间过长。 导致队列堵塞。 例如，内部的http请求没有增加timeout， 导致单次reconcile 时间超长。

可以考虑使用的统计和指标
- 不同controller reconcile 错误的次数在单位时间段内的统计。Histogram。

- 不同资源的reconcile的处理时间。Guage。
  devops_controller_xxx_reconcile_time_seconds{resource_name=xxxx}
  devops_controller_coderepository_reconcile_time_seconds{resource_name=devops-apiserver}

- rest client 请求的时间延迟 rest_client_request_latency_seconds
- rest client 请求的次数统计， 按照请求结果等统计 rest_client_requests_total

- workqueue 中 正在处理的资源个数： adds_total{name=xx}  , client-go 已实现
- workqueue 中剩余待处理的个数： depth{name=xxx} ， client-go 已实现
- workqueue 中的平均等待时间：queue_latency_seconds{name=xxx}, client-go 已实现
- workqueue 中的平均处理时间：work_duration_seconds{name=xxx}, client-go 已实现
- workqueue 中的重试总次数：retries_total， client-go 已实现
- longest_running_processor_microseconds ？


[x] admission webhooks server metrics

### Client-Go 也实现了一部分 指标
```go
	// client metrics
	requestLatency = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "rest_client_request_latency_seconds",
			Help:    "Request latency in seconds. Broken down by verb and URL.",
			Buckets: prometheus.ExponentialBuckets(0.001, 2, 10),
		},
		[]string{"verb", "url"},
	)

	requestResult = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "rest_client_requests_total",
			Help: "Number of HTTP requests, partitioned by status code, method, and host.",
		},
		[]string{"code", "method", "host"},
  )
  
  // reflector metrics
  

	// workqueue metrics

	workQueueSubsystem = "workqueue"

	depth = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Subsystem:   workQueueSubsystem,
		Name:        "depth",
		Help:        "Current depth of workqueue",
	}, []string{"name"})

	adds = prometheus.NewCounterVec(prometheus.CounterOpts{
		Subsystem:   workQueueSubsystem,
		Name:        "adds_total",
		Help:        "Total number of adds handled by workqueue",
	}, []string{"name"})

	latency = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Subsystem:   workQueueSubsystem,
		Name:        "queue_latency_seconds",
		Help:        "How long in seconds an item stays in workqueue before being requested.",
		Buckets:     prometheus.ExponentialBuckets(10e-9, 10, 10),
	}, []string{"name"})

	workDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Subsystem:   workQueueSubsystem,
		Name:        "work_duration_seconds",
		Help:        "How long in seconds processing an item from workqueue takes.",
		Buckets:     prometheus.ExponentialBuckets(10e-9, 10, 10),
	}, []string{"name"})

	retries = prometheus.NewCounterVec(prometheus.CounterOpts{
		Subsystem:   workQueueSubsystem,
		Name:        "retries_total",
		Help:        "Total number of retries handled by workqueue",
	}, []string{"name"})

	longestRunning = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Subsystem: workQueueSubsystem,
		Name:      "longest_running_processor_microseconds",
		Help: "How many microseconds has the longest running " +
			"processor for workqueue been running.",
	}, []string{"name"})

	unfinishedWork = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Subsystem: workQueueSubsystem,
		Name:      "unfinished_work_seconds",
		Help: "How many seconds of work has done that " +
			"is in progress and hasn't been observed by work_duration. Large " +
			"values indicate stuck threads. One can deduce the number of stuck " +
			"threads by observing the rate at which this increases.",
	}, []string{"name"})

```

## APIServer 指标监控
// k8s.io/apiserver@v0.0.0-20181213151703-3ccfe8365421/pkg/server/config.go:519
内部Confluence上规划的指标均是 APIServer的指标 和 k8s 默认提供的，有重复和缺失的项。
K8s APIServer 可以通过 enable-metrics 启用指标统计， 可以收集
- prometheus 的 go 相关指标。
- api endpoints 相关指标
  - 请求的次数统计
  - 请求时长超长的请求统计
  - 请求时间平均时长
  - 请求响应延迟摘要？apiserver_request_latencies_summary
  - 请求响应大小 apiserver_response_sizes
  - 请求需要try again的次数 apiserver_dropped_requests
  - watcher 个数  apiserver_registered_watchers
  - apiserver_current_inflight_requests ?
- 操作 etcd 的指标


```go
// k8s.io/apiserver@v0.0.0-20181213151703-3ccfe8365421/pkg/endpoints/metrics/metrics.go
requestCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "apiserver_request_count",
			Help: "Counter of apiserver requests broken out for each verb, API resource, client, and HTTP response contentType and code.",
		},
		[]string{"verb", "resource", "subresource", "scope", "client", "contentType", "code"},
	)
	longRunningRequestGauge = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "apiserver_longrunning_gauge",
			Help: "Gauge of all active long-running apiserver requests broken out by verb, API resource, and scope. Not all requests are tracked this way.",
		},
		[]string{"verb", "resource", "subresource", "scope"},
	)
	requestLatencies = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "apiserver_request_latencies",
			Help: "Response latency distribution in microseconds for each verb, resource and subresource.",
			// Use buckets ranging from 125 ms to 8 seconds.
			Buckets: prometheus.ExponentialBuckets(125000, 2.0, 7),
		},
		[]string{"verb", "resource", "subresource", "scope"},
	)
	requestLatenciesSummary = prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Name: "apiserver_request_latencies_summary",
			Help: "Response latency summary in microseconds for each verb, resource and subresource.",
			// Make the sliding window of 5h.
			// TODO: The value for this should be based on our SLI definition (medium term).
			MaxAge: 5 * time.Hour,
		},
		[]string{"verb", "resource", "subresource", "scope"},
	)
	responseSizes = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name: "apiserver_response_sizes",
			Help: "Response size distribution in bytes for each verb, resource, subresource and scope (namespace/cluster).",
			// Use buckets ranging from 1000 bytes (1KB) to 10^9 bytes (1GB).
			Buckets: prometheus.ExponentialBuckets(1000, 10.0, 7),
		},
		[]string{"verb", "resource", "subresource", "scope"},
	)
	// DroppedRequests is a number of requests dropped with 'Try again later' response"
	DroppedRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "apiserver_dropped_requests",
			Help: "Number of requests dropped with 'Try again later' response",
		},
		[]string{"requestKind"},
	)
	// RegisteredWatchers is a number of currently registered watchers splitted by resource.
	RegisteredWatchers = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "apiserver_registered_watchers",
			Help: "Number of currently registered watchers for a given resources",
		},
		[]string{"group", "version", "kind"},
	)
	// Because of volatality of the base metric this is pre-aggregated one. Instead of reporing current usage all the time
	// it reports maximal usage during the last second.
	currentInflightRequests = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "apiserver_current_inflight_requests",
			Help: "Maximal mumber of currently used inflight request limit of this apiserver per request kind in last second.",
		},
		[]string{"requestKind"},
	)
```