+++
title = "常见问题排查"
description = ""
weight = 5
alwaysopen = true
+++

### devops-controller无法正常启动

**描述**  
  devops-controller无法正常启动

**解释**  

**解决**  

### 查看当前资源的状态

**描述**  
   在排查问题的过程中，当前资源的信息上会保存很多有用的信息， 比如当前资源的状态和 message等等。  

**解决**  
  首先需要确认当前资源的类别(kind) 和所处的项目(namespace), 以及当前资源的名称。资源的类别定义参考 [核心概念](/concepts/overview#资源列表)
  知道这些之后，有两种办法  

  1. 在 global 集群上执行 `kubectl get {kind} {name} -n {namespace} -o yaml` 可以获得当前资源的 yaml 信息，这些信息中，在最后，可以看到一个 `status` 字段， 
  2. 点击 浏览器的 `审查元素` 按钮，点击 Network, 打开后， 点击当前资源的 更新按钮，（为了方便观察，点击前，可以点击 🚫清空当前网络请求）,   
    点击更新后， 观察当前网络请求中， 会有一个 GET {kind}/{namespace}/{name} 的请求。  
    点击 `Preview`, 可以看到 有个 `status` 字段。

 `status.phase` 中保存了当前资源的状态，`status.message` 保存了当前资源当前状态的一些提示信息。`status.reason` 中保存了一些详细的提示信息。