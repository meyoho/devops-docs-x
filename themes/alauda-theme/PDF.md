# PDF Generation (生产PDF)



为了支持更好的自定义生产PDF的方式需要：

- 创建PDF页面
- 生产PDF文件
- 在目标页面添加下载PDF链接



## 1. 创建PDF页面

文档网站可以通过markdown直接拼接一个完整的PDF



```
+++
# will not be visible from outside
hidden = true
+++


{{% pdfstyle %}}

{{% pdfcover title="My PDF" image="menu-entry-icon.png" %}}

{{% pdf page="shortcodes/children" %}}

```



### `pdfstyle` shortcode



此shortcode主要在页面上加打印PDF相关的css改动：

- 隐藏左导航/顶部
- 链接颜色
- 其它打印上的要求



### `pdfcover` shortcode

 为了自定义一个pdf的cover可以使用此shortcode。参数如下：



| 参数    | 默认值            | 描述          |
| ------- | ----------------- | ------------- |
| title   | title?            | cover中title  |
| image   | cover/cover.svg   | cover中的logo |
| bgImage | cover/coverbg.png | cover中背景图 |


### `pdf` shortcake

此shortcode主要功能是列出定义的路径中所有的页面和子页面以及它们的内容都放在一个页面上。不同的页面直接也会加分页。

**主意：列出所有的页面时会按照不同的文档中的 `weight` 字段执行排序**


| 参数    | 默认值            | 描述          |
| ------- | ----------------- | ------------- |
| page   | <empty>            | base展示内容的路径  |


example：
假如网站有两个大的标题：产品手册 (/manual) 和 API (/apidoc)，并且需要单独生产相关的PDF，那么可以在两个不同的页面添加 `page` 参数值为对应的路径，例如：

```
{{% pdf page="/apidoc" %}}
```



### 验证

页面写完了直接可以直接启动网站并直接访问路径来核对内容的展示

**主意：cover在直接用浏览器访问效果跟生产pdf不一样，如果有疑问可以先尝试生产pdf看效果**

假设 pdf markdown是 `content/pdf.md` 那么 直接可以使用 `/pdf`  路径访问

---



## 2. 生产pdf文件


可以使用 `kubectl-devops` 中的 `build-pdf` 命令：

```
kubectl devops apidocs build-pdf --page-numbers=<是否要加页数> --site-folder=<网站文件目录> --target-file=<生产pdf文件路径> --pdf-page-url=<pdf页面路径>
```

example:
```
kubectl devops apidocs build-pdf --page-numbers=false --site-folder=./ --target-file=static/pdf.pdf --pdf-page-url=/apidocs/pdf
```

### 其它帮助 

```
kubectl devops apidocs build-pdf --help
```

---



## 3. 在目标页面添加下载PDF链接

  1.  直接使用普通链接。
 2. 使用**附件**功能: 请见 [attachment](exampleSite/content/shortcodes/attachments.md)
 3.  其它方案，例如 button， 请见 [shortcodes](exampleSite/content/shortcodes)

