# Customizations 自定义

如果需要对alauda-theme执行自定义可以使用如下方案：

## Title

title在网站的 `config.toml` 文件中定义：

```

# 这里
[Languages]
[Languages.zh]
title = "内容例子网站"
weight = 1
languageName = "Chinese"

```

## Logo 

在自己的项目中的 `static` 目录创建  `images` 子目录并保存一个 `logo.svg` 文件： `static/images/logo.svg` 

可以参考 `exampleSite` 中的 [logo](exampleSite/static/images/logo.svg)

如果希望隐藏 logo 可以在网站的 `config.toml` 的`[params]`中加 `disableLogo = true` 参数

## Favicon

在自己的项目中的 `static` 目录创建  `images` 子目录并保存一个 `favicon.ico` 文件： `static/images/favicon.ico` 

可以参考 `exampleSite` 中的 [favicon](exampleSite/static/images/favicon.ico)


## Translations

在网站的仓库中添加 `i18n` 目录来保存翻译。[请见 hugo 文档](https://gohugo.io/content-management/multilingual)

如果要覆盖 theme 提供的翻译可以参考 theme 中的 `i18n` 目录

## Style

此theme提供三个不同的样式：`flex`, `original`, `tencent`. `exampleSite` 使用的是 `tencent`

如果需要修复或者修改样式需要修改相关的 `sass` 文件并执行构建:

```
make sass
```

其它 `make` 命令可以通过 `make help` 发现