# Hugo docDock Theme

This repository contains a theme for [Hugo](https://gohugo.io/), based on 

* [docDock theme](https://github.com/vjeantet/hugo-theme-docdock).
* [facette.io](https://facette.io/)'s documentation style css (Facette is a great time series data visualization software)



## 安装 kubectl-devops

请见 [CLI](CLI.md)



## Migration 迁移

原来文档都有自己的 theme，现在统一在alauda-theme实现，迁移和相关的步骤请见 [Migration](MIGRATION.md)



## API Documentation API文档

目前有API文档的支持，如何操作等信息请见 [API Docs](APIDOCS.md)



## PDF printing 生产PDF文件

打印PDF相关的请见 [PDF.md](PDF.md)



## Customizations 自定义/定制

如何定制网站或其它信息（例如 logo）请见 [Customizations](CUSTOMIZATIONS.md)



## Old readme

请见 [OLDREADME.md](OLDREADME.md)

