# API Docs （API文档）

API文档内容维护拆成不同的操作：

- 获取产品中 swagger 文档
- 生产/更改配置文件
- 生产合并的 swagger 文档
- 生产翻译文件 (source language)
- 在Weblate做翻译
- 生产文档库的翻译文件（hugo i18n)
- 生产API文档内容

关于具体技术方案，请见 [API文档生产方案](http://confluence.alauda.cn/x/9KQHAw)

[TOC]

## 1. 获取产品中 swagger 文档

分成集中：
- kubernetes自带的API文档, 包括其它APIServer
- CRD yaml 文件
- 高级API

### Kubernetes swagger

kubernetes自动会生产自己的swagger文档，并可以直接从它API获取. 不同的版本路径不一样 `/swagger.json` 或 `/openapi/v2/`

下载命令就得看环境上的配置以及用户的认证。可以协调运维或者研发帮助下载


### CRD

直接从研发拿相关的CRD文件。CRD的内容会影响生产文档的质量和准确性。建议参考 [cluster-registry](https://bitbucket.org/mathildetech/charts/src/master/alauda-base/templates/cluster/cluster-registry.yaml)

**关于CRD的内容准确性以及质量**： 如果发现了明显的错误或者不够友好的API文档需要提给对应研发owner来处理，要求改CRD描述。 这个Theme或者 `kubectl-devops` 跟CRD描述质量无关。


### 高级API

高级API如果使用 [alauda-backend](https://bitbucket.org/mathildetech/alauda-backend) 默认会提供一个 `/swagger.json` endpoint来获取swagger文档。可以跟研发协调怎么下载

**关于高级API的内容准确性以及质量**： 如果发现了明显的错误或者不够友好的API文档需要提给对应研发owner来处理，要求改高级API程序。 这个Theme或者 `kubectl-devops` 跟高级API质量无关。

---



## 2. 配置文件并生产合并的 swagger 文档

目前程序逻辑是：

 1. 读取所有的swagger文档 / CRD
 2. 合并所有的文档成一个，并加相关的前缀 `apiPathPrefix`
 3. 移除无用的API路径：根据 `generate.filters` 没有匹配的路径
 4. 合并后的文档中添加所有的 翻译 key
 5. 保存最终的文档到 `folderPath` 目录中为 `<version>`.json, 例如 `content/versions/v2.5.0.json`


配置文档结构如下，**请注意注释**：

```yaml
# source 是主要原文件
source:
  version: v2.5.0              # 当前 API 版本（产品版本）
  folderPath: content/versions # 保存最终合并过的文档
  files:                       # 声明所有的原swagger/crd文件
  - name: k8s                  # 名称，没有具体使用
    folderPath: swagger.json   # 文件路径
    apiPathPrefix: ""          # API路径前缀（增加固定的前缀）
  - name: devops
    folderPath: devops.json
    apiPathPrefix: "/devops"
  - name: crd
    folderPath: cluster-registry.yaml
    apiPathPrefix: ""

# 生产规则
generate:
# 一套可以有多个子目录， 可以有多套
- name: "kubernetes core api"                                      # 名称
  folderPath: "content/content/kubernetes"                         # API 文档 markdown文件目录路径
  filters:                                                         # 过滤 api 路径策略
  - apiPathRegex: "^/api/v1/namespaces/.namespace./secrets/.*"     # API 路径 regex 规则请参考  https://yourbasic.org/golang/regexp-cheat-sheet/  https://regex-golang.appspot.com/assets/html/index.html  https://www.golangprograms.com/regular-expressions.html
    folderPath: "secrets"                                          # 生成 markdown 子目录
- name: "devops api"
  folderPath: "content/content/devops"
  filters:
  - apiPathRegex: "^/apis/devops.alauda.io/v1alpha1/jenkinses/.*"
    folderPath: "jenkins"
  - apiPathRegex: "^/devops/.*"
    folderPath: "advanced"
    verbs:                                                         # 可以根据http verb过滤要生产的markdown文件
    - get
    - put
```

保存为一个 `yaml` 文件并执行： 

```
kubectl devops apidocs merge --config <config yaml文件>
```

example:

```
kubectl devops apidocs merge --config apidocs.yaml
```

执行 `merge`命令的过程当中如果`version`目标文件已经存在默认不会覆盖，需要加 `--force` 参数才会覆盖

上面的例子会：

**配置文件中所有的目录是根据当前执行命令的目录**

 1. 当前读取几个文件
   1.1. 读取 `swagger.json`
   1.2. 读取 `devops.json` swagger文档，修改内容加 `/devops` 前缀
   1.3. 读取 `cluster-registry.yaml` CRD 文件
 2. 合并所有的文档并存为 `content/versions/v2.5.0.json`，合并后会根据 `generate.filters` 过滤没有用到的 API
 3. 根据 `generate` 中 两个大目录结构生产：
   3.1 `content/content/kubernetes` 目录
     3.1.1 `content/content/kubernetes/secrets` 目录
     3.1.2 根据 `apiPathRegex` 在 `content/content/kubernetes/secrets` 目录中生产匹配到的API文档
   3.2 `content/content/devops` 目录 
     3.2.1 `content/content/devops/jenkins` 目录
     3.2.2 根据 `apiPathRegex` 过滤 API文档 并在 `content/content/devops/jenkins` 目录保存所有匹配的API文档
     3.2.3 `content/content/devops/advanced` 目录
     3.2.4 根据 `apiPathRegex` 和 `verbs` 过滤 API文档 并在 `content/content/devops/advanced` 目录保存所有匹配的API文档


### 建议：

1. 配置文件保存在 文档 仓库根目录，例如 `apidocs.yaml`
2. swagger文档文件以及CRD文件在单独目录维护，例如: `swagger/k8s.json` ,`swagger/devops.json`, `swagger/cluster-crd.yaml`
3. 高级API有明确的 `apiPathPrefix`, 可以跟研发确认，例如 devops产品的是 `/devops`
4. `generate`下第一级应该定义一个领域/功能模块，第二级（filters下）定义是一个功能模块，所以建议 `apiPathRegex` regex 匹配某个功能模块的具体API路径
5. **主意 `version` 中名字，必须跟产品版本一致**

### 其它帮助：

```
kubectl devops apidocs merge --help
```

---



## 3. 生产翻译文件 (source language) 

swagger文档所有的 `description` 都用英文描述，所以为了国际化文档的内容需要：

1. **生产source language 文件**
2. 提交到 weblate 来做翻译，并在weblate UI中翻译内容，weblate会提交到对应的代码仓库
3. 获取weblate中翻译过的文件，并生产`hugo`能使用的翻译文件

这部分会讲如何生产 source language文件

在 `2.` 同一个目录里面执行：

```
kubectl devops apidocs translate --config <config yaml文件> --output-file <目标文件>
```

example:

```
kubectl devops apidocs translate --config apidocs.yaml --output-file translations/en.json
```

### 建议：
 1. `output-file` 会生产英文 source 文件，所以使用 `en.json` 同步到 weblate 时会自动标记为 `English`
 2. 翻译文件的存储建议使用不同的代码仓库来维护，不然weblate中翻译改动会导致代码仓库提交并触发自动化流水线部署到 staging 环境。
 3. 如在别的代码仓库中维护需要在冻结代码后拉对应的分支。对文档工程师来说需要多学分支维护策略


### 其它帮助：

```
kubectl devops apidocs translate --help
```

---



## 4. 在weblate翻译并生产文档库的翻译文件（hugo i18n)

swagger文档所有的 `description` 都用英文描述，所以为了国际化文档的内容需要：

1. 生产source language 文件**
2. **提交到 weblate 来做翻译，并在weblate UI中翻译内容，weblate会提交到对应的代码仓库**
3. 获取weblate中翻译过的文件，并生产`hugo`能使用的翻译文件


weblate利用持续翻译的概念，并通过代码仓库不断的同步程序新的文案和国际化后的文案

对 weblate 的使用，配置以及其它相关操作请参考 [官方文档](https://docs.weblate.org/)

简单日常流程如下：
1. 生产 source file
2. 提交到翻译使用的代码仓库
3. 在weblate执行翻译，确认 weblate提交过翻译文件
4. 在本地 `git pull` 新的weblate提交
5. 在文档代码仓库中convert成`hugo` i18n文件, 这些文件必须在文档中的 `i18n` 目录

为了执行第五步，需要：

在文档目录中执行`convert`命令

```
kubectl devops apidocs convert --config <config yaml文件> --input-folder=<翻译文件目录> --output-folder=<i18n目录>
```

example

```
kubectl devops apidocs convert --config apidocs.yaml --input-folder=translations --output-folder=i18n
```

**主意：weblate生产的文件名字可能跟文档中的 `config.toml` 语言名字不一样，需要把`i18n`目录中的文件改成跟`config.toml`一致**, 例如：

```
defaultContentLanguage = "zh"
```




### 其它帮助：

```
kubectl devops apidocs convert --help
```

----



## 5. 生产API文档内容

最终步骤是生产文档中的 markdown文件:

 1. 在文档代码仓库中建 `data`目录
 2. 在目录中执行如下命令
 3. 程序会：
    3.1 自动拷贝当前版本 swagger文件（例如 `versions/v2.5.0.json`) 为 `data/swagger.json`并加相关的信息
    3.2 根据配置文件生产对应的 markdown文件，生产过程当中重复的文件不会被覆盖
 4. 文档中的示例可以通过`data`目录覆盖，但程序对data目录中的文件不会执行操作工程师可以


```
kubectl devops apidocs generate --config <配置yaml文件> --data-folder <data目录>
```

example

```
kubectl devops apidocs generate --config apidocs.yaml --data-folder data
```

### 如何覆盖请求体或返回体

程序会自动在swagger文档中生产默认请求的结构体，同时文档工程师可以覆盖 `示例` 展示的内容:

 1. 在`data`目录中创建跟API路径一样的结构，例如： 假设 API路径为 `/api/v1/some/api` 那么路径应该 `data/api/v1/some/api` 
    1.1. 如果路径特殊字符（例如 `{`）需要直接去掉
 2. 在对应的目录创建 HTTP Verb 目录, 例如 `data/api/v1/some/api/post`
 3. 如果需要覆盖请求体建 `request.json`，可以建默认返回体 `response.json`
 4. 返回体需要根据http状态码返回,一样创建对应的目录 `data/api/v1/some/api/post/201` 然后里面建 `response.json`
 5. 可以参考 `exampleSite/data` 里面的案例


### 建议

 1. 生产markdown 文件后需要调整一下 `title`, `description` 字段
 2. 认证或者别的全局文档需要手工生产/维护
 3. 文档仓库中的 `config.toml` 可以开启展示 缺少的翻译字段： `enableMissingTranslationPlaceholders = true`
 4. 尽量减少覆盖返回体或请求体，主要原因是避免不一致情况，增加维护成本


