+++
title = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} PATCH"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} PATCH"
weight = 10000
path = "PATCH /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="PATCH" %}}
