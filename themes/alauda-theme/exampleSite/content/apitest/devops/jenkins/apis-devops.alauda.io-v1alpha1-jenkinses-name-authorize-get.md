+++
title = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize GET"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize GET"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}/authorize" verb="GET" %}}
