+++
title = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} DELETE"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} DELETE"
weight = 10000
path = "DELETE /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="DELETE" %}}
