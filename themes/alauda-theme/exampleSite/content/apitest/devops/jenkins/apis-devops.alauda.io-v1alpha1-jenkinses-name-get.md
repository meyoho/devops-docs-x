+++
title = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} GET"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} GET"
weight = 10000
path = "GET /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="GET" %}}
