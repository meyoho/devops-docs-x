+++
title = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} PUT"
description = "/apis/devops.alauda.io/v1alpha1/jenkinses/{name} PUT"
weight = 10000
path = "PUT /apis/devops.alauda.io/v1alpha1/jenkinses/{name}"
+++


{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="PUT" %}}
