+++
title = "/devops/api/v1/configmap/{namespace} POST"
description = "/devops/api/v1/configmap/{namespace} POST"
weight = 10000
path = "POST /devops/api/v1/configmap/{namespace}"
+++


{{%api path="/devops/api/v1/configmap/{namespace}" verb="POST" %}}
