+++
title = "/devops/api/v1/clusterpipelinetemplate/{name}/exports GET"
description = "/devops/api/v1/clusterpipelinetemplate/{name}/exports GET"
weight = 10000
path = "GET /devops/api/v1/clusterpipelinetemplate/{name}/exports"
+++


{{%api path="/devops/api/v1/clusterpipelinetemplate/{name}/exports" verb="GET" %}}
