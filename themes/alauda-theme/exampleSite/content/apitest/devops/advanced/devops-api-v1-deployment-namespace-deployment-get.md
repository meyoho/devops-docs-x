+++
title = "/devops/api/v1/deployment/{namespace}/{deployment} GET"
description = "/devops/api/v1/deployment/{namespace}/{deployment} GET"
weight = 10000
path = "GET /devops/api/v1/deployment/{namespace}/{deployment}"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}" verb="GET" %}}
