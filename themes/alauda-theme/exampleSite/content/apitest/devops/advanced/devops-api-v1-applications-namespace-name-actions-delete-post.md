+++
title = "/devops/api/v1/applications/{namespace}/{name}/actions/delete POST"
description = "/devops/api/v1/applications/{namespace}/{name}/actions/delete POST"
weight = 10000
path = "POST /devops/api/v1/applications/{namespace}/{name}/actions/delete"
+++


{{%api path="/devops/api/v1/applications/{namespace}/{name}/actions/delete" verb="POST" %}}
