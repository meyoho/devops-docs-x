+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/replicas PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/replicas PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}/replicas"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/replicas" verb="PUT" %}}
