+++
title = "/devops/api/v1/secret/{namespace}/{name} DELETE"
description = "/devops/api/v1/secret/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/secret/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/secret/{namespace}/{name}" verb="DELETE" %}}
