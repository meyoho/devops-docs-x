+++
title = "/devops/api/v1/pipelinetemplatesync/{namespace}/{name} GET"
description = "/devops/api/v1/pipelinetemplatesync/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/pipelinetemplatesync/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/pipelinetemplatesync/{namespace}/{name}" verb="GET" %}}
