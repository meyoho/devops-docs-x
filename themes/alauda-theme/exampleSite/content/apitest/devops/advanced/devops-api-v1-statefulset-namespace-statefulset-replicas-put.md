+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/replicas PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/replicas PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}/replicas"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/replicas" verb="PUT" %}}
