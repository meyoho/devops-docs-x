+++
title = "/devops/api/v1/jenkinses/{name} DELETE"
description = "/devops/api/v1/jenkinses/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/jenkinses/{name}"
+++


{{%api path="/devops/api/v1/jenkinses/{name}" verb="DELETE" %}}
