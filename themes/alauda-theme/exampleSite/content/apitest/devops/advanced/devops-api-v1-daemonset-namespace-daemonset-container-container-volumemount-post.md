+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/volumeMount POST"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/volumeMount POST"
weight = 10000
path = "POST /devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/volumeMount"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/volumeMount" verb="POST" %}}
