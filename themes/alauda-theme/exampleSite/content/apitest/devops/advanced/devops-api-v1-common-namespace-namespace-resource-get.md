+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource} GET"
description = "/devops/api/v1/common/namespace/{namespace}/{resource} GET"
weight = 10000
path = "GET /devops/api/v1/common/namespace/{namespace}/{resource}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}" verb="GET" %}}
