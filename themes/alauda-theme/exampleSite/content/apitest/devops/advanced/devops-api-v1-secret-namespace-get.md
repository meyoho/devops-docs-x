+++
title = "/devops/api/v1/secret/{namespace} GET"
description = "/devops/api/v1/secret/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/secret/{namespace}"
+++


{{%api path="/devops/api/v1/secret/{namespace}" verb="GET" %}}
