+++
title = "/devops/api/v1/pipeline/{namespace}/{name}/testreports GET"
description = "/devops/api/v1/pipeline/{namespace}/{name}/testreports GET"
weight = 10000
path = "GET /devops/api/v1/pipeline/{namespace}/{name}/testreports"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}/testreports" verb="GET" %}}
