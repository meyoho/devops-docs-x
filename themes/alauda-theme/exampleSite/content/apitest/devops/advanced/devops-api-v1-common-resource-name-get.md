+++
title = "/devops/api/v1/common/{resource}/{name} GET"
description = "/devops/api/v1/common/{resource}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/common/{resource}/{name}"
+++


{{%api path="/devops/api/v1/common/{resource}/{name}" verb="GET" %}}
