+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset} GET"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset} GET"
weight = 10000
path = "GET /devops/api/v1/daemonset/{namespace}/{daemonset}"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}" verb="GET" %}}
