+++
title = "/devops/api/v1/codereposervice/{name} DELETE"
description = "/devops/api/v1/codereposervice/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/codereposervice/{name}"
+++


{{%api path="/devops/api/v1/codereposervice/{name}" verb="DELETE" %}}
