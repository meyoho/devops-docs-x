+++
title = "/devops/api/v1/pipelinetemplate/{namespace}/{name}/exports GET"
description = "/devops/api/v1/pipelinetemplate/{namespace}/{name}/exports GET"
weight = 10000
path = "GET /devops/api/v1/pipelinetemplate/{namespace}/{name}/exports"
+++


{{%api path="/devops/api/v1/pipelinetemplate/{namespace}/{name}/exports" verb="GET" %}}
