+++
title = "/devops/api/v1/pipelinetemplatesync/{namespace} GET"
description = "/devops/api/v1/pipelinetemplatesync/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/pipelinetemplatesync/{namespace}"
+++


{{%api path="/devops/api/v1/pipelinetemplatesync/{namespace}" verb="GET" %}}
