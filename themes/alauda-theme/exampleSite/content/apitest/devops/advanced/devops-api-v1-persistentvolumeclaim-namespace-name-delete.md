+++
title = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name} DELETE"
description = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/persistentvolumeclaim/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/persistentvolumeclaim/{namespace}/{name}" verb="DELETE" %}}
