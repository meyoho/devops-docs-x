+++
title = "/devops/api/v1/horizontalpodautoscaler/{namespace} GET"
description = "/devops/api/v1/horizontalpodautoscaler/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/horizontalpodautoscaler/{namespace}"
+++


{{%api path="/devops/api/v1/horizontalpodautoscaler/{namespace}" verb="GET" %}}
