+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub} POST"
description = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub} POST"
weight = 10000
path = "POST /devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub}" verb="POST" %}}
