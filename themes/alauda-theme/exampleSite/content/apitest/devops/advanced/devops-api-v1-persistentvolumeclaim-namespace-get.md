+++
title = "/devops/api/v1/persistentvolumeclaim/{namespace} GET"
description = "/devops/api/v1/persistentvolumeclaim/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/persistentvolumeclaim/{namespace}"
+++


{{%api path="/devops/api/v1/persistentvolumeclaim/{namespace}" verb="GET" %}}
