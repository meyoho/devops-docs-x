+++
title = "/devops/api/v1/pipeline/{namespace}/{name} GET"
description = "/devops/api/v1/pipeline/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/pipeline/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}" verb="GET" %}}
