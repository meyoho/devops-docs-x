+++
title = "/devops/api/v1/statistics/stage/{namespace} GET"
description = "/devops/api/v1/statistics/stage/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/statistics/stage/{namespace}"
+++


{{%api path="/devops/api/v1/statistics/stage/{namespace}" verb="GET" %}}
