+++
title = "/devops/api/v1/artifactregistrymanagers/{name} DELETE"
description = "/devops/api/v1/artifactregistrymanagers/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/artifactregistrymanagers/{name}"
+++


{{%api path="/devops/api/v1/artifactregistrymanagers/{name}" verb="DELETE" %}}
