+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/container/{container}/image PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/container/{container}/image PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}/container/{container}/image"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/container/{container}/image" verb="PUT" %}}
