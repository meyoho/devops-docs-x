+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container} PUT"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container} PUT"
weight = 10000
path = "PUT /devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}" verb="PUT" %}}
