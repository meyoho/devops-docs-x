+++
title = "/devops/api/v1/secret/{name} PUT"
description = "/devops/api/v1/secret/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/secret/{name}"
+++


{{%api path="/devops/api/v1/secret/{name}" verb="PUT" %}}
