+++
title = "/devops/api/v1/codequalityproject/{name} DELETE"
description = "/devops/api/v1/codequalityproject/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/codequalityproject/{name}"
+++


{{%api path="/devops/api/v1/codequalityproject/{name}" verb="DELETE" %}}
