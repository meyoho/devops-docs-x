+++
title = "/devops/api/v1/horizontalpodautoscaler/{namespace}/{name} PUT"
description = "/devops/api/v1/horizontalpodautoscaler/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/horizontalpodautoscaler/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/horizontalpodautoscaler/{namespace}/{name}" verb="PUT" %}}
