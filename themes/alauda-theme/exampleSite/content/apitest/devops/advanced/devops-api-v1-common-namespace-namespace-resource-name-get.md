+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name} GET"
description = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/common/namespace/{namespace}/{resource}/{name}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}/{name}" verb="GET" %}}
