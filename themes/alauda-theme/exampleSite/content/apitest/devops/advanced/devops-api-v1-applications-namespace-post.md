+++
title = "/devops/api/v1/applications/{namespace} POST"
description = "/devops/api/v1/applications/{namespace} POST"
weight = 10000
path = "POST /devops/api/v1/applications/{namespace}"
+++


{{%api path="/devops/api/v1/applications/{namespace}" verb="POST" %}}
