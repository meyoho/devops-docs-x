+++
title = "/devops/api/v1/applications/{namespace}/{name}/stop PUT"
description = "/devops/api/v1/applications/{namespace}/{name}/stop PUT"
weight = 10000
path = "PUT /devops/api/v1/applications/{namespace}/{name}/stop"
+++


{{%api path="/devops/api/v1/applications/{namespace}/{name}/stop" verb="PUT" %}}
