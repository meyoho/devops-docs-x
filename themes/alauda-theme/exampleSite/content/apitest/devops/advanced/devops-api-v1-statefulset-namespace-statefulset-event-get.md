+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/event GET"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/event GET"
weight = 10000
path = "GET /devops/api/v1/statefulset/{namespace}/{statefulset}/event"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/event" verb="GET" %}}
