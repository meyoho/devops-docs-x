+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/oldreplicaset GET"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/oldreplicaset GET"
weight = 10000
path = "GET /devops/api/v1/deployment/{namespace}/{deployment}/oldreplicaset"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/oldreplicaset" verb="GET" %}}
