+++
title = "/devops/api/v1/coderepobinding/{namespace}/{name}/repositories GET"
description = "/devops/api/v1/coderepobinding/{namespace}/{name}/repositories GET"
weight = 10000
path = "GET /devops/api/v1/coderepobinding/{namespace}/{name}/repositories"
+++


{{%api path="/devops/api/v1/coderepobinding/{namespace}/{name}/repositories" verb="GET" %}}
