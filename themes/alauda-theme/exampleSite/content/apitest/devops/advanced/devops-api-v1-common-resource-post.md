+++
title = "/devops/api/v1/common/{resource} POST"
description = "/devops/api/v1/common/{resource} POST"
weight = 10000
path = "POST /devops/api/v1/common/{resource}"
+++


{{%api path="/devops/api/v1/common/{resource}" verb="POST" %}}
