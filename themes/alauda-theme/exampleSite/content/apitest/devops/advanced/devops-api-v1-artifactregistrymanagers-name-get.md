+++
title = "/devops/api/v1/artifactregistrymanagers/{name} GET"
description = "/devops/api/v1/artifactregistrymanagers/{name} GET"
weight = 10000
path = "GET /devops/api/v1/artifactregistrymanagers/{name}"
+++


{{%api path="/devops/api/v1/artifactregistrymanagers/{name}" verb="GET" %}}
