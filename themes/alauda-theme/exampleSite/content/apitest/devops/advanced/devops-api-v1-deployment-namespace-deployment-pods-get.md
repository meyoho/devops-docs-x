+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/pods GET"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/pods GET"
weight = 10000
path = "GET /devops/api/v1/deployment/{namespace}/{deployment}/pods"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/pods" verb="GET" %}}
