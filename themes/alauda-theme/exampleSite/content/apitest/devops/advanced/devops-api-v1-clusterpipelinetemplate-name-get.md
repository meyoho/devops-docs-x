+++
title = "/devops/api/v1/clusterpipelinetemplate/{name} GET"
description = "/devops/api/v1/clusterpipelinetemplate/{name} GET"
weight = 10000
path = "GET /devops/api/v1/clusterpipelinetemplate/{name}"
+++


{{%api path="/devops/api/v1/clusterpipelinetemplate/{name}" verb="GET" %}}
