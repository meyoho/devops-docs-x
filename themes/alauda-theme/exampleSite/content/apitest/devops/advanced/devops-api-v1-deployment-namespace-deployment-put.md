+++
title = "/devops/api/v1/deployment/{namespace}/{deployment} PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment} PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}" verb="PUT" %}}
