+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/event GET"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/event GET"
weight = 10000
path = "GET /devops/api/v1/deployment/{namespace}/{deployment}/event"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/event" verb="GET" %}}
