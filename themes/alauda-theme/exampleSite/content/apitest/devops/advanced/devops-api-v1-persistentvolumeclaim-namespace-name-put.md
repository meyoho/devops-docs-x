+++
title = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name} PUT"
description = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/persistentvolumeclaim/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/persistentvolumeclaim/{namespace}/{name}" verb="PUT" %}}
