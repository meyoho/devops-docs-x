+++
title = "/devops/api/v1/secret/{name}/resources GET"
description = "/devops/api/v1/secret/{name}/resources GET"
weight = 10000
path = "GET /devops/api/v1/secret/{name}/resources"
+++


{{%api path="/devops/api/v1/secret/{name}/resources" verb="GET" %}}
