+++
title = "/devops/api/v1/configmap/{namespace}/{name} GET"
description = "/devops/api/v1/configmap/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/configmap/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/configmap/{namespace}/{name}" verb="GET" %}}
