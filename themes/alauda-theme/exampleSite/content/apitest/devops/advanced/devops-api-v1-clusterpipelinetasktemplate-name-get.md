+++
title = "/devops/api/v1/clusterpipelinetasktemplate/{name} GET"
description = "/devops/api/v1/clusterpipelinetasktemplate/{name} GET"
weight = 10000
path = "GET /devops/api/v1/clusterpipelinetasktemplate/{name}"
+++


{{%api path="/devops/api/v1/clusterpipelinetasktemplate/{name}" verb="GET" %}}
