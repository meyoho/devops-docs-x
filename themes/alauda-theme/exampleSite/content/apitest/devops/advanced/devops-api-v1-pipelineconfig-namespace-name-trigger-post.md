+++
title = "/devops/api/v1/pipelineconfig/{namespace}/{name}/trigger POST"
description = "/devops/api/v1/pipelineconfig/{namespace}/{name}/trigger POST"
weight = 10000
path = "POST /devops/api/v1/pipelineconfig/{namespace}/{name}/trigger"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}/{name}/trigger" verb="POST" %}}
