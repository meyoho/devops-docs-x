+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/pods GET"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/pods GET"
weight = 10000
path = "GET /devops/api/v1/statefulset/{namespace}/{statefulset}/pods"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/pods" verb="GET" %}}
