+++
title = "/devops/api/v1/artifactregistries/{name} GET"
description = "/devops/api/v1/artifactregistries/{name} GET"
weight = 10000
path = "GET /devops/api/v1/artifactregistries/{name}"
+++


{{%api path="/devops/api/v1/artifactregistries/{name}" verb="GET" %}}
