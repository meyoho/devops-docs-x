+++
title = "/devops/api/v1/codereposervice/{namespace}/{name}/resources GET"
description = "/devops/api/v1/codereposervice/{namespace}/{name}/resources GET"
weight = 10000
path = "GET /devops/api/v1/codereposervice/{namespace}/{name}/resources"
+++


{{%api path="/devops/api/v1/codereposervice/{namespace}/{name}/resources" verb="GET" %}}
