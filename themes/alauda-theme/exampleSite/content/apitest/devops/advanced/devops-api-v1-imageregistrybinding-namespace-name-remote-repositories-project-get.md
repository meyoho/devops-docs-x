+++
title = "/devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories-project GET"
description = "/devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories-project GET"
weight = 10000
path = "GET /devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories-project"
+++


{{%api path="/devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories-project" verb="GET" %}}
