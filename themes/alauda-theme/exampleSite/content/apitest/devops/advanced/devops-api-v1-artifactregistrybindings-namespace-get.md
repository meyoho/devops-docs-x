+++
title = "/devops/api/v1/artifactregistrybindings/{namespace} GET"
description = "/devops/api/v1/artifactregistrybindings/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/artifactregistrybindings/{namespace}"
+++


{{%api path="/devops/api/v1/artifactregistrybindings/{namespace}" verb="GET" %}}
