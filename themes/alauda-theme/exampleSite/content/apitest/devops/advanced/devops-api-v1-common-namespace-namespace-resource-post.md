+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource} POST"
description = "/devops/api/v1/common/namespace/{namespace}/{resource} POST"
weight = 10000
path = "POST /devops/api/v1/common/namespace/{namespace}/{resource}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}" verb="POST" %}}
