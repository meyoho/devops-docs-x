+++
title = "/devops/api/v1/pipelinetemplate/{namespace} GET"
description = "/devops/api/v1/pipelinetemplate/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/pipelinetemplate/{namespace}"
+++


{{%api path="/devops/api/v1/pipelinetemplate/{namespace}" verb="GET" %}}
