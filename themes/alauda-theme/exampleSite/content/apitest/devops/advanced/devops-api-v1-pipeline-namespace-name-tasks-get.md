+++
title = "/devops/api/v1/pipeline/{namespace}/{name}/tasks GET"
description = "/devops/api/v1/pipeline/{namespace}/{name}/tasks GET"
weight = 10000
path = "GET /devops/api/v1/pipeline/{namespace}/{name}/tasks"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}/tasks" verb="GET" %}}
