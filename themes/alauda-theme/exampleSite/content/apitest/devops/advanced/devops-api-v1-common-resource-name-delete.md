+++
title = "/devops/api/v1/common/{resource}/{name} DELETE"
description = "/devops/api/v1/common/{resource}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/common/{resource}/{name}"
+++


{{%api path="/devops/api/v1/common/{resource}/{name}" verb="DELETE" %}}
