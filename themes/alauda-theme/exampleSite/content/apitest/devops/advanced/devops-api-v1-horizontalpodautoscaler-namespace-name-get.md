+++
title = "/devops/api/v1/horizontalpodautoscaler/{namespace}/{name} GET"
description = "/devops/api/v1/horizontalpodautoscaler/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/horizontalpodautoscaler/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/horizontalpodautoscaler/{namespace}/{name}" verb="GET" %}}
