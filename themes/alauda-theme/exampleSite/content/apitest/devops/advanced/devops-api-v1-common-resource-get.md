+++
title = "/devops/api/v1/common/{resource} GET"
description = "/devops/api/v1/common/{resource} GET"
weight = 10000
path = "GET /devops/api/v1/common/{resource}"
+++


{{%api path="/devops/api/v1/common/{resource}" verb="GET" %}}
