+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub} GET"
description = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub} GET"
weight = 10000
path = "GET /devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}/{name}/sub/{sub}" verb="GET" %}}
