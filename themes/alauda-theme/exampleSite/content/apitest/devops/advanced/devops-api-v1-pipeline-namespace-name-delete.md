+++
title = "/devops/api/v1/pipeline/{namespace}/{name} DELETE"
description = "/devops/api/v1/pipeline/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/pipeline/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}" verb="DELETE" %}}
