+++
title = "/devops/api/v1/applications/{namespace}/{name}/yaml PUT"
description = "/devops/api/v1/applications/{namespace}/{name}/yaml PUT"
weight = 10000
path = "PUT /devops/api/v1/applications/{namespace}/{name}/yaml"
+++


{{%api path="/devops/api/v1/applications/{namespace}/{name}/yaml" verb="PUT" %}}
