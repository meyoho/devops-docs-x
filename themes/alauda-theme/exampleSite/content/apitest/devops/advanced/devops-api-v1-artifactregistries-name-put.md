+++
title = "/devops/api/v1/artifactregistries/{name} PUT"
description = "/devops/api/v1/artifactregistries/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/artifactregistries/{name}"
+++


{{%api path="/devops/api/v1/artifactregistries/{name}" verb="PUT" %}}
