+++
title = "/devops/api/v1/codequalitybinding/{namespace}/{name} PUT"
description = "/devops/api/v1/codequalitybinding/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/codequalitybinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/codequalitybinding/{namespace}/{name}" verb="PUT" %}}
