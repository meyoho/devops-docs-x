+++
title = "/devops/api/v1/pipeline/{namespace}/{name}/inputs POST"
description = "/devops/api/v1/pipeline/{namespace}/{name}/inputs POST"
weight = 10000
path = "POST /devops/api/v1/pipeline/{namespace}/{name}/inputs"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}/inputs" verb="POST" %}}
