+++
title = "/devops/api/v1/imageregistry/{name}/secrets GET"
description = "/devops/api/v1/imageregistry/{name}/secrets GET"
weight = 10000
path = "GET /devops/api/v1/imageregistry/{name}/secrets"
+++


{{%api path="/devops/api/v1/imageregistry/{name}/secrets" verb="GET" %}}
