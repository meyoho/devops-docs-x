+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name} DELETE"
description = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/common/namespace/{namespace}/{resource}/{name}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}/{name}" verb="DELETE" %}}
