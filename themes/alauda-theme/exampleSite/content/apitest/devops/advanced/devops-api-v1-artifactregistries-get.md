+++
title = "/devops/api/v1/artifactregistries GET"
description = "/devops/api/v1/artifactregistries GET"
weight = 10000
path = "GET /devops/api/v1/artifactregistries"
+++


{{%api path="/devops/api/v1/artifactregistries" verb="GET" %}}
