+++
title = "/devops/api/v1/pod/{namespace}/{name}/container GET"
description = "/devops/api/v1/pod/{namespace}/{name}/container GET"
weight = 10000
path = "GET /devops/api/v1/pod/{namespace}/{name}/container"
+++


{{%api path="/devops/api/v1/pod/{namespace}/{name}/container" verb="GET" %}}
