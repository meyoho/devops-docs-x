+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container} PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container} PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}" verb="PUT" %}}
