+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/volumeMount POST"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/volumeMount POST"
weight = 10000
path = "POST /devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/volumeMount"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/volumeMount" verb="POST" %}}
