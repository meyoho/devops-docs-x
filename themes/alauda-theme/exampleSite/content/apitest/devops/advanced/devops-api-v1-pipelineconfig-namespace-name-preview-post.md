+++
title = "/devops/api/v1/pipelineconfig/{namespace}/{name}/preview POST"
description = "/devops/api/v1/pipelineconfig/{namespace}/{name}/preview POST"
weight = 10000
path = "POST /devops/api/v1/pipelineconfig/{namespace}/{name}/preview"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}/{name}/preview" verb="POST" %}}
