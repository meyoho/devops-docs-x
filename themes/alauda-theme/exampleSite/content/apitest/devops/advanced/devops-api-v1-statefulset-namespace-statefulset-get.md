+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset} GET"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset} GET"
weight = 10000
path = "GET /devops/api/v1/statefulset/{namespace}/{statefulset}"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}" verb="GET" %}}
