+++
title = "/devops/api/v1/codequalityproject/{namespace} GET"
description = "/devops/api/v1/codequalityproject/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/codequalityproject/{namespace}"
+++


{{%api path="/devops/api/v1/codequalityproject/{namespace}" verb="GET" %}}
