+++
title = "/devops/api/v1/pipelineconfig/{namespace}/{name} PUT"
description = "/devops/api/v1/pipelineconfig/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/pipelineconfig/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}/{name}" verb="PUT" %}}
