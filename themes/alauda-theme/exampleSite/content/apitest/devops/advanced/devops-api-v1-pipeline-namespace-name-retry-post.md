+++
title = "/devops/api/v1/pipeline/{namespace}/{name}/retry POST"
description = "/devops/api/v1/pipeline/{namespace}/{name}/retry POST"
weight = 10000
path = "POST /devops/api/v1/pipeline/{namespace}/{name}/retry"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}/retry" verb="POST" %}}
