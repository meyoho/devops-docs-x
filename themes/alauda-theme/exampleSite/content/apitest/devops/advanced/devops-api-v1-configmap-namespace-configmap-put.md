+++
title = "/devops/api/v1/configmap/{namespace}/{configmap} PUT"
description = "/devops/api/v1/configmap/{namespace}/{configmap} PUT"
weight = 10000
path = "PUT /devops/api/v1/configmap/{namespace}/{configmap}"
+++


{{%api path="/devops/api/v1/configmap/{namespace}/{configmap}" verb="PUT" %}}
