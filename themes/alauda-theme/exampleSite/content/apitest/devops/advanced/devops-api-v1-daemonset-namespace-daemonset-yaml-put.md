+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/yaml PUT"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/yaml PUT"
weight = 10000
path = "PUT /devops/api/v1/daemonset/{namespace}/{daemonset}/yaml"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/yaml" verb="PUT" %}}
