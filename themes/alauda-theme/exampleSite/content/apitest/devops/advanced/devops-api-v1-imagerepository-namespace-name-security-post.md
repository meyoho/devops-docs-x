+++
title = "/devops/api/v1/imagerepository/{namespace}/{name}/security POST"
description = "/devops/api/v1/imagerepository/{namespace}/{name}/security POST"
weight = 10000
path = "POST /devops/api/v1/imagerepository/{namespace}/{name}/security"
+++


{{%api path="/devops/api/v1/imagerepository/{namespace}/{name}/security" verb="POST" %}}
