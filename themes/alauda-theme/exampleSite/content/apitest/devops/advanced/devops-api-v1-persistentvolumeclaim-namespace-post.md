+++
title = "/devops/api/v1/persistentvolumeclaim/{namespace} POST"
description = "/devops/api/v1/persistentvolumeclaim/{namespace} POST"
weight = 10000
path = "POST /devops/api/v1/persistentvolumeclaim/{namespace}"
+++


{{%api path="/devops/api/v1/persistentvolumeclaim/{namespace}" verb="POST" %}}
