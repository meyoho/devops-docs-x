+++
title = "/devops/api/v1/pipelineconfig/{namespace} POST"
description = "/devops/api/v1/pipelineconfig/{namespace} POST"
weight = 10000
path = "POST /devops/api/v1/pipelineconfig/{namespace}"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}" verb="POST" %}}
