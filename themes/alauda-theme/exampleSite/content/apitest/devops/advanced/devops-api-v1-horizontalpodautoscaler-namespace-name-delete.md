+++
title = "/devops/api/v1/horizontalpodautoscaler/{namespace}/{name} DELETE"
description = "/devops/api/v1/horizontalpodautoscaler/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/horizontalpodautoscaler/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/horizontalpodautoscaler/{namespace}/{name}" verb="DELETE" %}}
