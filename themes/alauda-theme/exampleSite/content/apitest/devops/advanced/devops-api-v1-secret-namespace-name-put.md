+++
title = "/devops/api/v1/secret/{namespace}/{name} PUT"
description = "/devops/api/v1/secret/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/secret/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/secret/{namespace}/{name}" verb="PUT" %}}
