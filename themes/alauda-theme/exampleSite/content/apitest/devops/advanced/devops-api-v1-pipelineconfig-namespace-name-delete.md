+++
title = "/devops/api/v1/pipelineconfig/{namespace}/{name} DELETE"
description = "/devops/api/v1/pipelineconfig/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/pipelineconfig/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}/{name}" verb="DELETE" %}}
