+++
title = "/devops/api/callback/oauth/{namespace}/secret/{secretNamespace}/{secretName}/codereposervice/{serviceName} GET"
description = "/devops/api/callback/oauth/{namespace}/secret/{secretNamespace}/{secretName}/codereposervice/{serviceName} GET"
weight = 10000
path = "GET /devops/api/callback/oauth/{namespace}/secret/{secretNamespace}/{secretName}/codereposervice/{serviceName}"
+++


{{%api path="/devops/api/callback/oauth/{namespace}/secret/{secretNamespace}/{secretName}/codereposervice/{serviceName}" verb="GET" %}}
