+++
title = "/devops/api/v1/jenkinsbinding/{namespace}/{name} PUT"
description = "/devops/api/v1/jenkinsbinding/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/jenkinsbinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/jenkinsbinding/{namespace}/{name}" verb="PUT" %}}
