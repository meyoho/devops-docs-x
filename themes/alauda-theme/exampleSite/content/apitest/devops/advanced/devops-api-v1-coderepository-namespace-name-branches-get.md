+++
title = "/devops/api/v1/coderepository/{namespace}/{name}/branches GET"
description = "/devops/api/v1/coderepository/{namespace}/{name}/branches GET"
weight = 10000
path = "GET /devops/api/v1/coderepository/{namespace}/{name}/branches"
+++


{{%api path="/devops/api/v1/coderepository/{namespace}/{name}/branches" verb="GET" %}}
