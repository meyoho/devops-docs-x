+++
title = "/devops/api/v1/jenkinsbinding/{namespace}/{name}/labels GET"
description = "/devops/api/v1/jenkinsbinding/{namespace}/{name}/labels GET"
weight = 10000
path = "GET /devops/api/v1/jenkinsbinding/{namespace}/{name}/labels"
+++


{{%api path="/devops/api/v1/jenkinsbinding/{namespace}/{name}/labels" verb="GET" %}}
