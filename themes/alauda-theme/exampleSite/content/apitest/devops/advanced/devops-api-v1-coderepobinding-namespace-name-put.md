+++
title = "/devops/api/v1/coderepobinding/{namespace}/{name} PUT"
description = "/devops/api/v1/coderepobinding/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/coderepobinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/coderepobinding/{namespace}/{name}" verb="PUT" %}}
