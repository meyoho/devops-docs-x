+++
title = "/devops/api/v1/imagerepository/{namespace} GET"
description = "/devops/api/v1/imagerepository/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/imagerepository/{namespace}"
+++


{{%api path="/devops/api/v1/imagerepository/{namespace}" verb="GET" %}}
