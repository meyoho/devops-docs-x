+++
title = "/devops/api/v1/secret/{name} DELETE"
description = "/devops/api/v1/secret/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/secret/{name}"
+++


{{%api path="/devops/api/v1/secret/{name}" verb="DELETE" %}}
