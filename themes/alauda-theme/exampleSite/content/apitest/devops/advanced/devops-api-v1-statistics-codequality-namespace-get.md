+++
title = "/devops/api/v1/statistics/codequality/{namespace} GET"
description = "/devops/api/v1/statistics/codequality/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/statistics/codequality/{namespace}"
+++


{{%api path="/devops/api/v1/statistics/codequality/{namespace}" verb="GET" %}}
