+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/image PUT"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/image PUT"
weight = 10000
path = "PUT /devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/image"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/image" verb="PUT" %}}
