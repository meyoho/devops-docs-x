+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/network PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/network PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}/network"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/network" verb="PUT" %}}
