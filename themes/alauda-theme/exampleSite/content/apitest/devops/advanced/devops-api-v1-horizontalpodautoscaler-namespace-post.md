+++
title = "/devops/api/v1/horizontalpodautoscaler/{namespace} POST"
description = "/devops/api/v1/horizontalpodautoscaler/{namespace} POST"
weight = 10000
path = "POST /devops/api/v1/horizontalpodautoscaler/{namespace}"
+++


{{%api path="/devops/api/v1/horizontalpodautoscaler/{namespace}" verb="POST" %}}
