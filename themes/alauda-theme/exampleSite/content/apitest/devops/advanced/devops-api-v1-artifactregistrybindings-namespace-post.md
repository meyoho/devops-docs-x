+++
title = "/devops/api/v1/artifactregistrybindings/{namespace} POST"
description = "/devops/api/v1/artifactregistrybindings/{namespace} POST"
weight = 10000
path = "POST /devops/api/v1/artifactregistrybindings/{namespace}"
+++


{{%api path="/devops/api/v1/artifactregistrybindings/{namespace}" verb="POST" %}}
