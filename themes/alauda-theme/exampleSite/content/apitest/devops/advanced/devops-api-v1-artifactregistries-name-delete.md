+++
title = "/devops/api/v1/artifactregistries/{name} DELETE"
description = "/devops/api/v1/artifactregistries/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/artifactregistries/{name}"
+++


{{%api path="/devops/api/v1/artifactregistries/{name}" verb="DELETE" %}}
