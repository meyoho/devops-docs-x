+++
title = "/devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories GET"
description = "/devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories GET"
weight = 10000
path = "GET /devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories"
+++


{{%api path="/devops/api/v1/imageregistrybinding/{namespace}/{name}/remote-repositories" verb="GET" %}}
