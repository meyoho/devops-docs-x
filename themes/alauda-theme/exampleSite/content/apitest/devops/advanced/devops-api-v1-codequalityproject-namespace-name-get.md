+++
title = "/devops/api/v1/codequalityproject/{namespace}/{name} GET"
description = "/devops/api/v1/codequalityproject/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/codequalityproject/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/codequalityproject/{namespace}/{name}" verb="GET" %}}
