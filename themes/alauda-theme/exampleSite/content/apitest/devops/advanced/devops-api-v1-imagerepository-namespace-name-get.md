+++
title = "/devops/api/v1/imagerepository/{namespace}/{name} GET"
description = "/devops/api/v1/imagerepository/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/imagerepository/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/imagerepository/{namespace}/{name}" verb="GET" %}}
