+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/resources PUT"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/resources PUT"
weight = 10000
path = "PUT /devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/resources"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/container/{container}/resources" verb="PUT" %}}
