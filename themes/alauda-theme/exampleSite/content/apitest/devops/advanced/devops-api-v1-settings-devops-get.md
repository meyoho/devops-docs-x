+++
title = "/devops/api/v1/settings/devops GET"
description = "/devops/api/v1/settings/devops GET"
weight = 10000
path = "GET /devops/api/v1/settings/devops"
+++


{{%api path="/devops/api/v1/settings/devops" verb="GET" %}}
