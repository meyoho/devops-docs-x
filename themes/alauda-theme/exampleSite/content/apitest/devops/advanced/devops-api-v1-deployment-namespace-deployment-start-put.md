+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/start PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/start PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}/start"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/start" verb="PUT" %}}
