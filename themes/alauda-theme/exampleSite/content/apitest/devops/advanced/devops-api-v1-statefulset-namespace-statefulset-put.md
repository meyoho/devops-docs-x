+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset} PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset} PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}" verb="PUT" %}}
