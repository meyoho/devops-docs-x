+++
title = "/devops/api/v1/artifactregistrybindings/{namespace}/{name} DELETE"
description = "/devops/api/v1/artifactregistrybindings/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/artifactregistrybindings/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/artifactregistrybindings/{namespace}/{name}" verb="DELETE" %}}
