+++
title = "/devops/api/v1/codereposervice/{name} GET"
description = "/devops/api/v1/codereposervice/{name} GET"
weight = 10000
path = "GET /devops/api/v1/codereposervice/{name}"
+++


{{%api path="/devops/api/v1/codereposervice/{name}" verb="GET" %}}
