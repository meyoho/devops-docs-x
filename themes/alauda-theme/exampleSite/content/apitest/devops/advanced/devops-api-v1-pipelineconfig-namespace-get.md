+++
title = "/devops/api/v1/pipelineconfig/{namespace} GET"
description = "/devops/api/v1/pipelineconfig/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/pipelineconfig/{namespace}"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}" verb="GET" %}}
