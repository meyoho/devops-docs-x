+++
title = "/devops/api/v1/common/{resource}/{name} PUT"
description = "/devops/api/v1/common/{resource}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/common/{resource}/{name}"
+++


{{%api path="/devops/api/v1/common/{resource}/{name}" verb="PUT" %}}
