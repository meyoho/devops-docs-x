+++
title = "/devops/api/v1/configmap/{namespace}/{configmap} DELETE"
description = "/devops/api/v1/configmap/{namespace}/{configmap} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/configmap/{namespace}/{configmap}"
+++


{{%api path="/devops/api/v1/configmap/{namespace}/{configmap}" verb="DELETE" %}}
