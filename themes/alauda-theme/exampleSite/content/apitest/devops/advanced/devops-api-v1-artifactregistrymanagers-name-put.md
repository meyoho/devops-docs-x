+++
title = "/devops/api/v1/artifactregistrymanagers/{name} PUT"
description = "/devops/api/v1/artifactregistrymanagers/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/artifactregistrymanagers/{name}"
+++


{{%api path="/devops/api/v1/artifactregistrymanagers/{name}" verb="PUT" %}}
