+++
title = "/devops/api/v1/coderepobinding/{namespace}/{name} GET"
description = "/devops/api/v1/coderepobinding/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/coderepobinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/coderepobinding/{namespace}/{name}" verb="GET" %}}
