+++
title = "/devops/api/v1/toolchain/bindings/{namespace} GET"
description = "/devops/api/v1/toolchain/bindings/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/toolchain/bindings/{namespace}"
+++


{{%api path="/devops/api/v1/toolchain/bindings/{namespace}" verb="GET" %}}
