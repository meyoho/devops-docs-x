+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/image PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/image PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/image"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/image" verb="PUT" %}}
