+++
title = "/devops/api/v1/codequalitybinding/{namespace}/{name}/projects GET"
description = "/devops/api/v1/codequalitybinding/{namespace}/{name}/projects GET"
weight = 10000
path = "GET /devops/api/v1/codequalitybinding/{namespace}/{name}/projects"
+++


{{%api path="/devops/api/v1/codequalitybinding/{namespace}/{name}/projects" verb="GET" %}}
