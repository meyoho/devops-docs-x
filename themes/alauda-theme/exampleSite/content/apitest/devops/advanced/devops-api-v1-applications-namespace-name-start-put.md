+++
title = "/devops/api/v1/applications/{namespace}/{name}/start PUT"
description = "/devops/api/v1/applications/{namespace}/{name}/start PUT"
weight = 10000
path = "PUT /devops/api/v1/applications/{namespace}/{name}/start"
+++


{{%api path="/devops/api/v1/applications/{namespace}/{name}/start" verb="PUT" %}}
