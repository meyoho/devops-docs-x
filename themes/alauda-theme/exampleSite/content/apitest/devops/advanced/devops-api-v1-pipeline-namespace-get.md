+++
title = "/devops/api/v1/pipeline/{namespace} GET"
description = "/devops/api/v1/pipeline/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/pipeline/{namespace}"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}" verb="GET" %}}
