+++
title = "/devops/api/v1/statistics/pipeline/{namespace} GET"
description = "/devops/api/v1/statistics/pipeline/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/statistics/pipeline/{namespace}"
+++


{{%api path="/devops/api/v1/statistics/pipeline/{namespace}" verb="GET" %}}
