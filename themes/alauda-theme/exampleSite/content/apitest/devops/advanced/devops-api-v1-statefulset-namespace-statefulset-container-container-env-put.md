+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/env PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/env PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/env"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/container/{container}/env" verb="PUT" %}}
