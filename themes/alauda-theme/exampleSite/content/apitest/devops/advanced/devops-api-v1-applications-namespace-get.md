+++
title = "/devops/api/v1/applications/{namespace} GET"
description = "/devops/api/v1/applications/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/applications/{namespace}"
+++


{{%api path="/devops/api/v1/applications/{namespace}" verb="GET" %}}
