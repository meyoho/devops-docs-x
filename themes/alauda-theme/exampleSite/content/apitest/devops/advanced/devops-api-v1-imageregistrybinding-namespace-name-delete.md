+++
title = "/devops/api/v1/imageregistrybinding/{namespace}/{name} DELETE"
description = "/devops/api/v1/imageregistrybinding/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/imageregistrybinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/imageregistrybinding/{namespace}/{name}" verb="DELETE" %}}
