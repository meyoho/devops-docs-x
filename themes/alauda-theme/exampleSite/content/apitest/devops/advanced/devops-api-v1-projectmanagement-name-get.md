+++
title = "/devops/api/v1/projectmanagement/{name} GET"
description = "/devops/api/v1/projectmanagement/{name} GET"
weight = 10000
path = "GET /devops/api/v1/projectmanagement/{name}"
+++


{{%api path="/devops/api/v1/projectmanagement/{name}" verb="GET" %}}
