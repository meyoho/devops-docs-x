+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/yaml PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/yaml PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}/yaml"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/yaml" verb="PUT" %}}
