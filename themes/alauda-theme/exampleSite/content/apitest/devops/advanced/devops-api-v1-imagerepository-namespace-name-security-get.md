+++
title = "/devops/api/v1/imagerepository/{namespace}/{name}/security GET"
description = "/devops/api/v1/imagerepository/{namespace}/{name}/security GET"
weight = 10000
path = "GET /devops/api/v1/imagerepository/{namespace}/{name}/security"
+++


{{%api path="/devops/api/v1/imagerepository/{namespace}/{name}/security" verb="GET" %}}
