+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/stop PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/stop PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}/stop"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/stop" verb="PUT" %}}
