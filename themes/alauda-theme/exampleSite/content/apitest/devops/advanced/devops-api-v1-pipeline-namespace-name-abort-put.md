+++
title = "/devops/api/v1/pipeline/{namespace}/{name}/abort PUT"
description = "/devops/api/v1/pipeline/{namespace}/{name}/abort PUT"
weight = 10000
path = "PUT /devops/api/v1/pipeline/{namespace}/{name}/abort"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}/abort" verb="PUT" %}}
