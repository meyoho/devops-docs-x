+++
title = "/devops/api/v1/codequalitybinding/{namespace}/{name} DELETE"
description = "/devops/api/v1/codequalitybinding/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/codequalitybinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/codequalitybinding/{namespace}/{name}" verb="DELETE" %}}
