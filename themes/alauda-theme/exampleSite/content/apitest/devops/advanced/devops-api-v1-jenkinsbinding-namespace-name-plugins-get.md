+++
title = "/devops/api/v1/jenkinsbinding/{namespace}/{name}/plugins GET"
description = "/devops/api/v1/jenkinsbinding/{namespace}/{name}/plugins GET"
weight = 10000
path = "GET /devops/api/v1/jenkinsbinding/{namespace}/{name}/plugins"
+++


{{%api path="/devops/api/v1/jenkinsbinding/{namespace}/{name}/plugins" verb="GET" %}}
