+++
title = "/devops/api/v1/pipeline/{namespace}/{name}/logs GET"
description = "/devops/api/v1/pipeline/{namespace}/{name}/logs GET"
weight = 10000
path = "GET /devops/api/v1/pipeline/{namespace}/{name}/logs"
+++


{{%api path="/devops/api/v1/pipeline/{namespace}/{name}/logs" verb="GET" %}}
