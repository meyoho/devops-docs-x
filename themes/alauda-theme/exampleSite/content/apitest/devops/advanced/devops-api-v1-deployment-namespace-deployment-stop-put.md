+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/stop PUT"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/stop PUT"
weight = 10000
path = "PUT /devops/api/v1/deployment/{namespace}/{deployment}/stop"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/stop" verb="PUT" %}}
