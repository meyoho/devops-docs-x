+++
title = "/devops/api/v1/log/{namespace}/{pod} GET"
description = "/devops/api/v1/log/{namespace}/{pod} GET"
weight = 10000
path = "GET /devops/api/v1/log/{namespace}/{pod}"
+++


{{%api path="/devops/api/v1/log/{namespace}/{pod}" verb="GET" %}}
