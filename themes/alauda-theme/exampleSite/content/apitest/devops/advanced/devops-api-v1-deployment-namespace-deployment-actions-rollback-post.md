+++
title = "/devops/api/v1/deployment/{namespace}/{deployment}/actions/rollback POST"
description = "/devops/api/v1/deployment/{namespace}/{deployment}/actions/rollback POST"
weight = 10000
path = "POST /devops/api/v1/deployment/{namespace}/{deployment}/actions/rollback"
+++


{{%api path="/devops/api/v1/deployment/{namespace}/{deployment}/actions/rollback" verb="POST" %}}
