+++
title = "/devops/api/v1/imageregistry/{name} DELETE"
description = "/devops/api/v1/imageregistry/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/imageregistry/{name}"
+++


{{%api path="/devops/api/v1/imageregistry/{name}" verb="DELETE" %}}
