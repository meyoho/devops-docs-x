+++
title = "/devops/api/v1/coderepository/{namespace} GET"
description = "/devops/api/v1/coderepository/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/coderepository/{namespace}"
+++


{{%api path="/devops/api/v1/coderepository/{namespace}" verb="GET" %}}
