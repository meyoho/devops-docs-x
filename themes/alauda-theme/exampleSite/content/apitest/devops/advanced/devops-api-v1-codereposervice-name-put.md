+++
title = "/devops/api/v1/codereposervice/{name} PUT"
description = "/devops/api/v1/codereposervice/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/codereposervice/{name}"
+++


{{%api path="/devops/api/v1/codereposervice/{name}" verb="PUT" %}}
