+++
title = "/devops/api/v1/log/file/{namespace}/{pod}/{container} GET"
description = "/devops/api/v1/log/file/{namespace}/{pod}/{container} GET"
weight = 10000
path = "GET /devops/api/v1/log/file/{namespace}/{pod}/{container}"
+++


{{%api path="/devops/api/v1/log/file/{namespace}/{pod}/{container}" verb="GET" %}}
