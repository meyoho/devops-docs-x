+++
title = "/devops/api/v1/coderepobinding/{namespace}/{name} DELETE"
description = "/devops/api/v1/coderepobinding/{namespace}/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/coderepobinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/coderepobinding/{namespace}/{name}" verb="DELETE" %}}
