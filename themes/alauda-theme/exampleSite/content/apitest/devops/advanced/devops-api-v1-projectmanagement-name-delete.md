+++
title = "/devops/api/v1/projectmanagement/{name} DELETE"
description = "/devops/api/v1/projectmanagement/{name} DELETE"
weight = 10000
path = "DELETE /devops/api/v1/projectmanagement/{name}"
+++


{{%api path="/devops/api/v1/projectmanagement/{name}" verb="DELETE" %}}
