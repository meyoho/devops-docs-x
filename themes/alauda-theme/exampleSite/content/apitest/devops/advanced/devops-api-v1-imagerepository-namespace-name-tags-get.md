+++
title = "/devops/api/v1/imagerepository/{namespace}/{name}/tags GET"
description = "/devops/api/v1/imagerepository/{namespace}/{name}/tags GET"
weight = 10000
path = "GET /devops/api/v1/imagerepository/{namespace}/{name}/tags"
+++


{{%api path="/devops/api/v1/imagerepository/{namespace}/{name}/tags" verb="GET" %}}
