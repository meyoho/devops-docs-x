+++
title = "/devops/api/v1/codequalitybinding/{namespace}/{name}/secrets GET"
description = "/devops/api/v1/codequalitybinding/{namespace}/{name}/secrets GET"
weight = 10000
path = "GET /devops/api/v1/codequalitybinding/{namespace}/{name}/secrets"
+++


{{%api path="/devops/api/v1/codequalitybinding/{namespace}/{name}/secrets" verb="GET" %}}
