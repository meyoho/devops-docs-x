+++
title = "/devops/api/v1/toolchain/bindings GET"
description = "/devops/api/v1/toolchain/bindings GET"
weight = 10000
path = "GET /devops/api/v1/toolchain/bindings"
+++


{{%api path="/devops/api/v1/toolchain/bindings" verb="GET" %}}
