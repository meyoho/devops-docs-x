+++
title = "/devops/api/v1/storageclass/{name} GET"
description = "/devops/api/v1/storageclass/{name} GET"
weight = 10000
path = "GET /devops/api/v1/storageclass/{name}"
+++


{{%api path="/devops/api/v1/storageclass/{name}" verb="GET" %}}
