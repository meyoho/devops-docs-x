+++
title = "/devops/api/v1/pipelineconfig/{namespace}/{name}/scan POST"
description = "/devops/api/v1/pipelineconfig/{namespace}/{name}/scan POST"
weight = 10000
path = "POST /devops/api/v1/pipelineconfig/{namespace}/{name}/scan"
+++


{{%api path="/devops/api/v1/pipelineconfig/{namespace}/{name}/scan" verb="POST" %}}
