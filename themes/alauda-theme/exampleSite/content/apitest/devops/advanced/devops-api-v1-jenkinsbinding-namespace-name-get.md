+++
title = "/devops/api/v1/jenkinsbinding/{namespace}/{name} GET"
description = "/devops/api/v1/jenkinsbinding/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/jenkinsbinding/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/jenkinsbinding/{namespace}/{name}" verb="GET" %}}
