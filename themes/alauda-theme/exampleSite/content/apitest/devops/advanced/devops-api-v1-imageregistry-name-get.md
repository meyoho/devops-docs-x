+++
title = "/devops/api/v1/imageregistry/{name} GET"
description = "/devops/api/v1/imageregistry/{name} GET"
weight = 10000
path = "GET /devops/api/v1/imageregistry/{name}"
+++


{{%api path="/devops/api/v1/imageregistry/{name}" verb="GET" %}}
