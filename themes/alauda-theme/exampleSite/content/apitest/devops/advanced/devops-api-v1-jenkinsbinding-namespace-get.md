+++
title = "/devops/api/v1/jenkinsbinding/{namespace} GET"
description = "/devops/api/v1/jenkinsbinding/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/jenkinsbinding/{namespace}"
+++


{{%api path="/devops/api/v1/jenkinsbinding/{namespace}" verb="GET" %}}
