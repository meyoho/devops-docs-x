+++
title = "/devops/api/v1/imageregistrybinding/{namespace}/{name}/repositories GET"
description = "/devops/api/v1/imageregistrybinding/{namespace}/{name}/repositories GET"
weight = 10000
path = "GET /devops/api/v1/imageregistrybinding/{namespace}/{name}/repositories"
+++


{{%api path="/devops/api/v1/imageregistrybinding/{namespace}/{name}/repositories" verb="GET" %}}
