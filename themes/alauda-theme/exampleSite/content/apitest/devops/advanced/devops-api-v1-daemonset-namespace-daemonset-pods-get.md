+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/pods GET"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/pods GET"
weight = 10000
path = "GET /devops/api/v1/daemonset/{namespace}/{daemonset}/pods"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/pods" verb="GET" %}}
