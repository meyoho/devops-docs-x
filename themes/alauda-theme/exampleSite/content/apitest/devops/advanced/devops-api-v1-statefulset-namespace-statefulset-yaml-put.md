+++
title = "/devops/api/v1/statefulset/{namespace}/{statefulset}/yaml PUT"
description = "/devops/api/v1/statefulset/{namespace}/{statefulset}/yaml PUT"
weight = 10000
path = "PUT /devops/api/v1/statefulset/{namespace}/{statefulset}/yaml"
+++


{{%api path="/devops/api/v1/statefulset/{namespace}/{statefulset}/yaml" verb="PUT" %}}
