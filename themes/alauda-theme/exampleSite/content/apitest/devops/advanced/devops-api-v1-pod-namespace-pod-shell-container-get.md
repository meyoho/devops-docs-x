+++
title = "/devops/api/v1/pod/{namespace}/{pod}/shell/{container} GET"
description = "/devops/api/v1/pod/{namespace}/{pod}/shell/{container} GET"
weight = 10000
path = "GET /devops/api/v1/pod/{namespace}/{pod}/shell/{container}"
+++


{{%api path="/devops/api/v1/pod/{namespace}/{pod}/shell/{container}" verb="GET" %}}
