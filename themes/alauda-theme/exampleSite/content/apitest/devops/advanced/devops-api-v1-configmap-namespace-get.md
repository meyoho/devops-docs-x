+++
title = "/devops/api/v1/configmap/{namespace} GET"
description = "/devops/api/v1/configmap/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/configmap/{namespace}"
+++


{{%api path="/devops/api/v1/configmap/{namespace}" verb="GET" %}}
