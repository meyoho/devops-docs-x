+++
title = "/devops/api/v1/applications/{namespace}/{name} PUT"
description = "/devops/api/v1/applications/{namespace}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/applications/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/applications/{namespace}/{name}" verb="PUT" %}}
