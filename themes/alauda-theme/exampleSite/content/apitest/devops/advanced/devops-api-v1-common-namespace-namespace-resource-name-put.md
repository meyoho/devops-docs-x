+++
title = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name} PUT"
description = "/devops/api/v1/common/namespace/{namespace}/{resource}/{name} PUT"
weight = 10000
path = "PUT /devops/api/v1/common/namespace/{namespace}/{resource}/{name}"
+++


{{%api path="/devops/api/v1/common/namespace/{namespace}/{resource}/{name}" verb="PUT" %}}
