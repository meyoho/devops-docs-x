+++
title = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name}/actions/tradeapp POST"
description = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name}/actions/tradeapp POST"
weight = 10000
path = "POST /devops/api/v1/persistentvolumeclaim/{namespace}/{name}/actions/tradeapp"
+++


{{%api path="/devops/api/v1/persistentvolumeclaim/{namespace}/{name}/actions/tradeapp" verb="POST" %}}
