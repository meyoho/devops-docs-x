+++
title = "/devops/api/v1/applications/{namespace}/{name}/yaml GET"
description = "/devops/api/v1/applications/{namespace}/{name}/yaml GET"
weight = 10000
path = "GET /devops/api/v1/applications/{namespace}/{name}/yaml"
+++


{{%api path="/devops/api/v1/applications/{namespace}/{name}/yaml" verb="GET" %}}
