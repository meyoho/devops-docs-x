+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset} PUT"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset} PUT"
weight = 10000
path = "PUT /devops/api/v1/daemonset/{namespace}/{daemonset}"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}" verb="PUT" %}}
