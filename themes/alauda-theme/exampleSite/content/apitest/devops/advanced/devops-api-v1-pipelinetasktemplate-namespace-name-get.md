+++
title = "/devops/api/v1/pipelinetasktemplate/{namespace}/{name} GET"
description = "/devops/api/v1/pipelinetasktemplate/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/pipelinetasktemplate/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/pipelinetasktemplate/{namespace}/{name}" verb="GET" %}}
