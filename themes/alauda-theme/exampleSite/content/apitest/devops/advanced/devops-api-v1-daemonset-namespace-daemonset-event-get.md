+++
title = "/devops/api/v1/daemonset/{namespace}/{daemonset}/event GET"
description = "/devops/api/v1/daemonset/{namespace}/{daemonset}/event GET"
weight = 10000
path = "GET /devops/api/v1/daemonset/{namespace}/{daemonset}/event"
+++


{{%api path="/devops/api/v1/daemonset/{namespace}/{daemonset}/event" verb="GET" %}}
