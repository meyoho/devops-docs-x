+++
title = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name} GET"
description = "/devops/api/v1/persistentvolumeclaim/{namespace}/{name} GET"
weight = 10000
path = "GET /devops/api/v1/persistentvolumeclaim/{namespace}/{name}"
+++


{{%api path="/devops/api/v1/persistentvolumeclaim/{namespace}/{name}" verb="GET" %}}
