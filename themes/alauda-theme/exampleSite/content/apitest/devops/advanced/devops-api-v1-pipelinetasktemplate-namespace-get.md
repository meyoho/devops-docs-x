+++
title = "/devops/api/v1/pipelinetasktemplate/{namespace} GET"
description = "/devops/api/v1/pipelinetasktemplate/{namespace} GET"
weight = 10000
path = "GET /devops/api/v1/pipelinetasktemplate/{namespace}"
+++


{{%api path="/devops/api/v1/pipelinetasktemplate/{namespace}" verb="GET" %}}
