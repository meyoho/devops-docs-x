+++
title = "/api/v1/namespaces/{namespace}/configmaps/{name} PATCH"
description = "/api/v1/namespaces/{namespace}/configmaps/{name} PATCH"
weight = 10000
path = "PATCH /api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/configmaps/{name}" verb="PATCH" %}}
