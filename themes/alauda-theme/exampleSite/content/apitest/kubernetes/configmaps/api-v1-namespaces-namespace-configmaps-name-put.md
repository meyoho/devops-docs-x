+++
title = "/api/v1/namespaces/{namespace}/configmaps/{name} PUT"
description = "/api/v1/namespaces/{namespace}/configmaps/{name} PUT"
weight = 10000
path = "PUT /api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/configmaps/{name}" verb="PUT" %}}
