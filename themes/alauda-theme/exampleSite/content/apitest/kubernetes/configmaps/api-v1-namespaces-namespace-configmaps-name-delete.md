+++
title = "/api/v1/namespaces/{namespace}/configmaps/{name} DELETE"
description = "/api/v1/namespaces/{namespace}/configmaps/{name} DELETE"
weight = 10000
path = "DELETE /api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/configmaps/{name}" verb="DELETE" %}}
