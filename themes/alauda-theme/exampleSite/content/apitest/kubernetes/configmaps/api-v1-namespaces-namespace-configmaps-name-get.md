+++
title = "/api/v1/namespaces/{namespace}/configmaps/{name} GET"
description = "/api/v1/namespaces/{namespace}/configmaps/{name} GET"
weight = 10000
path = "GET /api/v1/namespaces/{namespace}/configmaps/{name}"
+++


{{%api path="/api/v1/namespaces/{namespace}/configmaps/{name}" verb="GET" %}}
