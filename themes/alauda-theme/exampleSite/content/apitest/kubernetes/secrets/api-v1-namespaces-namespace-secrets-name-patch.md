+++
title = "/api/v1/namespaces/{namespace}/secrets/{name} PATCH"
description = "/api/v1/namespaces/{namespace}/secrets/{name} PATCH"
weight = 10000
path = "PATCH /api/v1/namespaces/{namespace}/secrets/{name}"
type = "json"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="PATCH" %}}
