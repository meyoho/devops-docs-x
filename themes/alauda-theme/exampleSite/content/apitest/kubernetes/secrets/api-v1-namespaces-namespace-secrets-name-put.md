+++
title = "/api/v1/namespaces/{namespace}/secrets/{name} PUT"
description = "/api/v1/namespaces/{namespace}/secrets/{name} PUT"
weight = 10000
path = "PUT /api/v1/namespaces/{namespace}/secrets/{name}"
type = "json"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="PUT" %}}
