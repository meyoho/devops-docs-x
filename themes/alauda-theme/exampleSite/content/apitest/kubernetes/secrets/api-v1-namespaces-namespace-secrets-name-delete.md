+++
title = "/api/v1/namespaces/{namespace}/secrets/{name} DELETE"
description = "/api/v1/namespaces/{namespace}/secrets/{name} DELETE"
weight = 10000
path = "DELETE /api/v1/namespaces/{namespace}/secrets/{name}"
type = "json"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="DELETE" %}}
