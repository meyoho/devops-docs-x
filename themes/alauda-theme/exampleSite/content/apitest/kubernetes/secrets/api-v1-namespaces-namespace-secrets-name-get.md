+++
title = "/api/v1/namespaces/{namespace}/secrets/{name} GET"
description = "/api/v1/namespaces/{namespace}/secrets/{name} GET"
weight = 10000
path = "GET /api/v1/namespaces/{namespace}/secrets/{name}"
type = "json"
+++


{{%api path="/api/v1/namespaces/{namespace}/secrets/{name}" verb="GET" %}}
