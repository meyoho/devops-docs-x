+++
title = "apichildren"
description = "The APIchildren shortcode displays an list of apichildren."
tags = ["api"]
+++


Use the apichildren shortcode to list child pages and ther further descendants that are 
API pages


## Usage


| Parameter | Default | Description |
|:--|:--|:--|
| page | _current_ | Specify the page name (section name) to display children for |
| showhidden | "false" | When true, child pages hidden from the menu will be displayed |
| depth | 1 | Enter a number to specify the depth of descendants to display. For example, if the value is 2, the shortcode will display 2 levels of child pages. set 999 for all |
| sort | none | Sort Children By<br><li><strong>Weight</strong> - to sort on menu order</li><li><strong>Name</strong> - to sort alphabetically on menu label</li><li><strong>Identifier</strong> - to sort alphabetically on identifier set in frontmatter</li><li><strong>URL</strong> - URL</li> |


{{% alert theme="warning"%}}the page should have a <code>api = "GET /api/path"</code> in the contents header{{% /alert %}}

```
+++ 
title = "some page"
path = "GET /version"
+++

# content
```

## Demo

    {{%/* apichildren  */%}}

{{% apichildren %}}



## Page Browsing

    {{%/* children page="content-organisation" depth=2 style="table" */%}}


{{% alert theme="warning"%}}
Please check `children` for more information on this `shortcode` functionality
{{% /alert %}}


## 用户手册 

{{% children page="content-organisation" depth=2 style="table" %}}

