+++
title = "Retrieve one someapi"
description = "/myapi/test/someapi/{name} get"
tags = ["api"]
path = "GET /myapi/test/someapi/{name}"
+++


{{% api path="/myapi/test/someapi/{name}" verb="get" %}}
