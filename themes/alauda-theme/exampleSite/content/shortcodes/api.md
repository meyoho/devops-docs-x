+++
title = "api"
description = "The API shortcode displays an API documentation."
tags = ["api"]
+++


In order to add a api description to the website there are a few requirements:

- there is a `data` folder in your website
- there is a `data/swagger.json` swagger 2.0 api document
- the required data is present inside the document


## Usage

| Parameter | Default | Description |
|:--|:--|:--|
| path | _empty_ | the path of the api resource, e.g `/myapi/path` |
| verb | _empty_ | the verb needed for the api, e.g `get` |

    {{%/* api path="/myapi/path" verb="get" */%}}


## Demo

    {{%/*api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="PUT" */%}}

{{%api path="/apis/devops.alauda.io/v1alpha1/jenkinses/{name}" verb="PUT" %}}