+++

path = "GET /devops/api/appConfig.json"
hidden = true
+++

{{% pdfstyle %}}

{{% pdfcover title="My PDF" image="menu-entry-icon.png" %}}

{{% pdf page="shortcodes/children" %}}

{{% pdf page="apitest/devops/jenkins" %}}

{{% pdf page="apitest/devops/advanced/devops-api-v1-applications-namespace-post" %}}

