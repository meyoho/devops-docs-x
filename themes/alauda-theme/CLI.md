## 1. 安装 docker

需要使用Docker CLI，Docker Desktop自带可以使用的：[Docker for Desktop](https://www.docker.com/products/docker-desktop)

---



## 2. 安装 kubectl-devops

安装 CLI 工具来执行文档相关的操作：

 - 启动文档网站
 - 构建网站
 - 构建PDF文件
 - API 文档相关的操作，请见[API Docs](APIDOCS.md)

可以通过 `kubectl` 访问 `kubectl-devops` : `kubectl devops` 

也可以直接使用 `kubect-devops` 

- 如何安装 [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

- 访问 [kubectl-devops downloads](https://bitbucket.org/mathildetech/kubectl-devops/downloads/) 


如果在Unix系统中需要手工 move 成 `$PATH` 中为 `kubectl-devops`， 如下如何直接下载并move 到 `$PATH` 在`macOS` 上执行命令：
**主意：bitbucket用户名不是邮箱地址，而是你个人仓库中路径**

```
curl -u <bitbucket 用户名>:<bitbucket密码> -v -OL https://api.bitbucket.org/2.0/repositories/mathildetech/kubectl-devops/downloads/kubectl-devops-mac
chmod +x kubectl-devops-mac
sudo mv kubectl-devops-mac /usr/local/bin/kubectl-devops

```

安装成功后可以用如下两个命令来验证：

`kubectl devops` 或 `kubectl-devops` 