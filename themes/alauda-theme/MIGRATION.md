# Migration (迁移)

[TOC]

## 1. 安装需要的工具

请见 [CLI](CLI.md)



## 2. 修改config

在`config.toml`文件中执行如下改动。主意：项目中有否多个`config.toml`?

原`config.toml`:

**主意：网站展示左侧的title可以通过 `title` 字段设置**

```
[Languages]
[Languages.zh]
title = "DevOps用户文档"


[params]
...
themeStyle = "flex" 

```

改成：

```
[params]
...
themeStyle = "tencent"
```

---



## 3. 启动网站

通过kubectl-devops可以启动网站，尝试一下 alauda-theme:

在文档的根目录中执行

```
kubectl-devops apidocs run
```

看其它可以定制的参数请执行

```
kubectl-devops apidocs run --help
```

---



## 3. 构建网站

**主意：很少见在本地构建，一般 启动网站就可以在本地验证。**
为了构建最终的网站可以执行 `kubectl devops apidocs build` 命令


```
kubectl devops apidocs build --site-folder=<文档目录路径> --config-file=<生产最终网站的config.toml文件>
```


example

```
kubectl devops apidocs build --site-folder=./ --config-file=artifacts/config.toml
```

### 其它帮助

```
kubectl devops apidocs build --help
```

---



## 4. 生产PDF

请见 [PDF](PDF.md)



## 5. Dockerfile和流水线改动

Dockerfile 可以使用 [multi-stage build](https://docs.docker.com/develop/develop-images/multistage-build/) 来解决构建时使用alauda-theme，可以参考`devops-docs` 的实现：

```dockerfile
FROM index.alauda.cn/alaudak8s/alauda-theme:latest as builder

COPY . /content

WORKDIR /content

RUN hugo --ignoreCache \
  --ignoreVendor \
  --config config.toml \
  --cleanDestinationDir \
  --theme=alauda-theme \
  --themesDir=/ \
  -s /content

FROM index.alauda.cn/alaudaorg/alaudabase-alpine-run:alpine3.10

RUN apk add --no-cache --virtual .build-deps openssh-client git tar curl; \
    curl --silent --show-error --fail --location --header "Accept: application/tar+gzip, application/x-gzip, application/octet-stream" -o - \
      "https://caddyserver.com/download/linux/amd64?license=personal&telemetry=off" \
    | tar --no-same-owner -C /usr/bin/ -xz caddy && \
    chmod 0755 /usr/bin/caddy && \
    addgroup -S caddy && \
    adduser -D -S -H -s /sbin/nologin -G caddy caddy && \
    /usr/bin/caddy -version && \
    apk del .build-deps;
ENV PORT=80 \
    SITE_ROOT=/public
ENTRYPOINT ["/usr/bin/caddy"]
CMD ["-conf", "/Caddyfile", "-root", "${SITE_ROOT}"]
COPY artifacts/Caddyfile /
COPY --from=builder /content/public /public
COPY --from=builder /content /original


```



**主意：建议在dockerfile里面拷贝整个网站所有的文件到一个`/original` 目录上，这样如果需要在私有环境上做定制可以直接从镜像里面拷贝原文件**

Jenkinsfile的改动主要是去掉在Jenkins中构建的部分



## 6. 删除原有的 Theme

在文档的跟目录中删除 `themes` 目录

```
rm -rf themes/hugo-theme-docdock
```

