# ==============================================================================
# Makefile helper functions for sass
#

SASS := sass
NODE := node
SASS_SUPPORTED_VERSION ?= 3.7|3.6
NODE_SUPPORTED_VERSIONS ?= v10|v8
STYLES ?= flex original tencent

.PHONY: node.build.verify
node.build.verify:
ifneq ($(shell $(NODE) --version | grep -q -E '\b($(NODE_SUPPORTED_VERSIONS))\b' && echo 0 || echo 1), 0)
	$(error unsupported node version. Please make install one of the following supported version: '$(NODE_SUPPORTED_VERSIONS)')
endif

.PHONY: sass.build.verify
sass.build.verify: node.build.verify
ifneq ($(shell $(SASS) --version | grep -q -E '\bSass ($(SASS_SUPPORTED_VERSION))\b' && echo 0 || echo 1), 0)
	$(error unsupported sass version. Please make install one of the following supported version: '$(SASS_SUPPORTED_VERSION)')
endif

.PHONY: sass.build.%
sass.build.%:
	$(eval STYLE := $(word 1,$(subst ., ,$*)))
	@echo "===========> Building sass file $(STYLE): $(wildcard static/scss/$(word 1,$(subst ., ,$*))/style.scss)"
	sass static/scss/$(STYLE)/style.scss:static/theme-$(STYLE)/style.css || true

.PHONE: sass.build
sass.build: $(foreach p,$(STYLES),$(addprefix sass.build., $(p))) 
