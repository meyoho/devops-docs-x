# ==============================================================================
# Makefile helper functions for hugo
#


HUGO := hugo
HUGO_SUPPORTED_VERSIONS ?= v0.48|v0.50|v0.58|v0.60


.PHONY: hugo.build.verify
hugo.build.verify:
ifneq ($(shell $(HUGO) version | grep -q -E '\b($(HUGO_SUPPORTED_VERSIONS))\b' && echo 0 || echo 1), 0)
	$(error unsupported hugo version. Please make install one of the following supported version: '$(HUGO_SUPPORTED_VERSIONS)')
endif
