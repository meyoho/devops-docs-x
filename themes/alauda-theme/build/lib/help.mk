# ==============================================================================
# Help

define HELPTEXT
Usage: make <OPTIONS> ... <TARGETS>

Targets:
    sass               Build sass css files for all themes.
    run                Run hugo using the exampleSite for development.
    clean              Remove all sass cache files.
    help               Show this help info.

Options:
    DEBUG        Whether to generate debug symbols. Default is 0.
    V            Set to 1 enable verbose build. Default is 0.

endef
export HELPTEXT

