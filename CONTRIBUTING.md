## 环境初始化

执行命令 `make setup`

## 预览

执行命令 `make run`

## 快速上手

本例中，以书写流水线功能的， 图形化创建流水线为例，设计文档为例，给出说明

- 创建一个图形化创建流水线的过程中的设计方案文档
```
hugo new --kind design feature/pipeline/design/graph-pipeline.md
```
这个命令会在 content/feature/pipeline/design 下增加一个 graph-pipeline.md 文件， 使用的模板是 archetypes/design.md.

- 修改内容
正常修改 feature/pipeline/design/graph-pipeline.md 文档的内容，如果某个章节下没有内容， 则注明 `无` 即可。

- 预览效果
执行 `make run`, 访问 http://localhost:1313


## 常用命令

- 新增 功能设计文档: `hugo new --kind feature feature/{功能模块名称}/{文件名称}.md`
- 新增 过程中的设计文档: `hugo new --kind design feature/{功能模块名称}/design/{文件名称}.md`
- 新增 功能排查文档: `hugo new --kind troubleshooting feature/{功能模块名称}/troubleshooting.md`

## 注意 ⚠️ ⚠️ ⚠️
 
1. ⚠️ 不要随意在 content 下增加顶级目录
2. ⚠️ 不要随意在 content/feature 下增加子模块目录
3. ⚠️ content/feature/{功能模块} 下的文件， 需要保证随功能更新，保证时效和正确性。
4. ⚠️ 平时我们写的，某个技术方案的设计，放在 content/feature/{功能模块}/design 下。这里只需要聚焦当前设计即可。 不需要保证时效性。

## 目录结构说明

参考 [目录结构说明](/content/_index.md )


