// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
def deployment
// image can be used for promoting...
def IMAGE
def IMAGE_E2E
def CURRENT_VERSION
def RELEASE_VERSION
def RELEASE_BUILD
def code_data
def DEBUG = false
def release
def docs

pipeline {
	agent { label 'golang-1.12' }

	options {
		buildDiscarder(logRotator(numToKeepStr: '10'))
		disableConcurrentBuilds()
		skipDefaultCheckout()
	}
	parameters {
		booleanParam(name: 'DEBUG', defaultValue: false, description: 'Debug will not do final changes...')
	}
	environment {
		FOLDER = "."

		// for building an scanning
		REPOSITORY = "devops-docs-x"
		OWNER = "mathildetech"
		// sonar feedback user
		// needs to change together with the credentialsID
		BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
		SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
		DINGDING_BOT = "devops-chat-bot"
		TAG_CREDENTIALS = "alaudabot-bitbucket"
		GOPATH = "${WORKSPACE}"
		IMAGE_REPOSITORY = "devops/devops-docs-x"
		// charts pipeline name
		CHARTS_PIPELINE = "/common/common-charts-pipeline"
		CHART_NAME = "alauda-devops"
		CHART_COMPONENT = "docsx"

		HUGONF_IMAGE = "index.alauda.cn/devops/hugonf:latest"
		CONFLUENCE_SPACE = "DEVOPS"
		CONFLUENCE_SERVER = "http://confluence.alauda.cn"
		CONFLUENCE_SECRET = "devops-confluence"
	}
	// stages
	stages {
		stage('Checkout') {
			steps {
				script {
					container('tools') {
						// checkout code
						def scmVars
						retry(2) {
							scmVars = checkout scm
						}
									GIT_BRANCH = scmVars.GIT_BRANCH
						release = deploy.release(scmVars)
						docs = deploy.Docs(scmVars)
						
						RELEASE_BUILD = release.version
						RELEASE_VERSION = release.majorVersion
						// echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
						echo """
							release ${RELEASE_VERSION}
							version ${release.version}
							is_release ${release.is_release}
							is_build ${release.is_build}
							is_master ${release.is_master}
							deploy_env ${release.environment}
							auto_test ${release.auto_test}
							environment ${release.environment}
							majorVersion ${release.majorVersion}
						"""
						// copying kubectl from tools
						sh "cp /usr/local/bin/kubectl ."
					}
				}
			}
		}

		stage('Merge Docs'){
			steps{
				script{
					docs.merge_docs()
				}
			}
		}

		stage('Build Image'){
			steps {
				script {
					container('tools') {
						// currently is building code inside the container
						IMAGE = deploy.dockerBuildWithRegister(
							dockerfile: "artifacts/Dockerfile", //Dockerfile
							address: IMAGE_REPOSITORY, // repo address
							tag: RELEASE_BUILD,
							armBuild: false
						)
						// start and push
						IMAGE.start().push()
					}
				}
			}
		}
		// after build it should start deploying
		stage('Tag git') {
			when {
				expression {
					release.shouldTag()
				}
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							deploy.gitTag(
								TAG_CREDENTIALS,
								RELEASE_BUILD,
								OWNER,
								REPOSITORY
								)
						}
					}
				}
			}
		}
		stage('Chart Update') {
			when {
				expression {
					release.shouldUpdateChart()
				}
			}
			steps {
				script {
					deploy.triggerChart([
						chart: CHART_NAME,
						component: CHART_COMPONENT,
						version: RELEASE_VERSION,
						imageTag: RELEASE_BUILD,
						branch: release.chartBranch,
						prBranch: release.change["branch"],
						env: release.environment
					]).start()
				}
			}
		}
		stage('Publish to Confluence') {
			when {
				expression {
					GIT_BRANCH == "master" || GIT_BRANCH ==~ /release-\d+\.\d+/
				}
			}
			steps{
				script{
					container("tools"){

							withCredentials([usernamePassword(credentialsId: CONFLUENCE_SECRET, passwordVariable: 'CONF_PASSWORD', usernameVariable: 'CONF_USERNAME')]) {
								docker.image(HUGONF_IMAGE).inside{
										sh "echo 139.186.34.240  confluence.alauda.cn>> /etc/hosts"
										sh """
												pwd && ls -la
												hugonf sync --v=3 --binary /opt/atlassian-cli-9.1.0/acli.sh --space ${CONFLUENCE_SPACE} --hugo ./ --config config.toml --configDir ./ --server ${CONFLUENCE_SERVER} --username ${CONF_USERNAME} --password ${CONF_PASSWORD} --rootSuffix=@${GIT_BRANCH} --parent=技术文档.auto
											"""
								}
							}
						}
					}

				}
			}
	}

	// (optional)
	// happens at the end of the pipeline
	post {
		// 成功
		success {
			script {
				container('tools') {
					def msg = "流水线成功啦🍻🍻🍻"
					deploy.notificationSuccess(REPOSITORY, DINGDING_BOT, msg, RELEASE_BUILD)
					if (release != null) { release.cleanEnv() }
				}
			}
		}
		// 失败
		failure {
			script {
				container('tools') {
					deploy.notificationFailed(REPOSITORY, DINGDING_BOT, "流水线失败了💔", RELEASE_BUILD)
					if (release != null) { release.cleanEnv() }
				}
			}
		}
	}
}
