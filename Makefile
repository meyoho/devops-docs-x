PWD=$(shell pwd)
TAG=dev-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/devops-docs-x
image=$(IMAGE)
tag=$(TAG)
HUGO=${GOPATH}/src/github.com/gohugoio/hugo
DEVOPS=${GOPATH}/src/bitbucket.org/mathildetech/kubectl-devops

.PHONE: setup
setup:
	@if [ -a $(HUGO) ] ; \
	then \
			cd $(HUGO) && git pull && git checkout feat/add-jsonify-pretty; \
	else \
			git clone --branch feat/add-jsonify-pretty https://github.com/alauda/hugo $(HUGO); \
	fi;

	cd $(HUGO) && go install .
	which hugo && hugo version
	@if [ -a $(DEVOPS) ] ; \
	then \
			cd $(DEVOPS) && git pull; \
	else \
			git clone https://bitbucket.org/mathildetech/kubectl-devops $(DEVOPS); \
	fi;
	cd $(DEVOPS)/cmd/kubectl-devops && go install .
	which kubectl-devops && kubectl devops version

run:
	kubectl devops docs run

build-site:
	kubectl devops docs build

build-docker:
	# cp artifacts/Caddyfile public
	docker build -t ${IMAGE}:${TAG} -f artifacts/Dockerfile .
	docker push ${IMAGE}:${TAG}

build: build-docker

build-pdf:
	kubectl devops docs build-pdf --pdf-page-url /aggregation/sdd --target-file static/sdd.pdf
	kubectl devops docs build-pdf --pdf-page-url /aggregation/troubleshooting --target-file static/troubleshooting.pdf

build-doc-file:
	./scripts/gen-doc.sh sdd http://localhost:1313/aggregation/sdd.docx/

publish-confluence:
	kubectl devops docs publish-confluence --server http://confluence.alauda.cn --username xxx --password xxx --space DEMO