#!/bin/bash
set -ex

# ./gen-doc.sh sdd http://localhost:1313/aggregation/sdd.origin/
# $1 should be doc file name that you want to save, without suffix
# $2 should be markdown web page

FILE_NAME=$1
URL=$2
if [ "${FILE_NAME}" == "" ];then
  echo "invalid args, eg. ./gen-doc.sh sdd http://localhost:1313/aggregation/sdd.origin/"
  exit 1
fi

curl ${URL} > ${FILE_NAME}.md
sed -i -e  's^](/images^](./static/images^g' ./sdd.md
sed -i -e '/confluence.alauda.cn/d' ./sdd.md
pandoc ${FILE_NAME}.md --dpi 300 --toc --verbose -f markdown -t docx -s -o ./static/${FILE_NAME}.docx
rm ${FILE_NAME}.md || true
rm ${FILE_NAME}.md-e || true
